<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config = array(
                 'providers/add' => array(
                                    array(
                                            'field' => 'name',
                                            'label' => 'Name ',
                                            'rules' => 'trim|required|xss_clean'
                                         )
                          
                                    ),
				 
                        'login' => array(
                                    array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|min_length[6]'
                                         ),
									  
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email',
                                            'rules' => 'required|valid_email|callback_email_check'
                                         )
                                    
                                    ),
                    'members/view_edit' => array(
                                            array(
                                                'field' => 'name',
                                                'label' => 'Name',
                                                'rules' => 'required'
                                            ),
                                            array(
                                                'field' => 'email',
                                                'label' => 'Email'
                                            ),
                                            array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|min_length[6]'
                                            ),
                                            array(
                                                    'field' => 'passconf',
                                                    'label' => 'Password Confirmation',
                                                    'rules' => 'required|matches[password]'
                                                 )
                                    ),
                    'users/view_edit' => array(
                                            array(
                                                'field' => 'name',
                                                'label' => 'Name',
                                                'rules' => 'required'
                                            ),
                                            array(
                                                'field' => 'email',
                                                'label' => 'Email'
                                            ),
                                            array(
                                            'field' => 'phone',
                                            'label' => 'Phone'
                                            ),
                                            array(
                                                    'field' => 'vip',
                                                    'label' => 'VIP'
                                                 )
                                    ),

					'members/addnew' => array(
                                    array(
                                            'field' => 'name',
                                            'label' => 'fullname ',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|min_length[6]'
											
                                         ),
									 
                                    array(
                                            'field' => 'passconf',
                                            'label' => 'Password Confirmation',
                                            'rules' => 'required|matches[password]'
                                         ),
									 
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email',
                                            'rules' => 'required|valid_email|callback_email_check'
                                         )
                                    )                       
               );
//$config = array(
//           'member/signup' => array(
//                                    array(
//                                            'field' => 'username',
//                                            'label' => 'Username',
//                                            'rules' => 'required'
//                                         ),
//                                    array(
//                                            'field' => 'password',
//                                            'label' => 'Password',
//                                            'rules' => 'required|min_length[6]'
//											
//                                         ),
//									 
//                                    array(
//                                            'field' => 'passconf',
//                                            'label' => 'Password Confirmation',
//                                            'rules' => 'required|matches[password]'
//                                         ),
//                                    array(
//                                            'field' => 'email',
//                                            'label' => 'Email',
//                                            'rules' => 'required|valid_email'
//                                         )
//                                    )
//               );		