<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members extends CI_Controller {
	
	public function __construct()
    {
            parent::__construct();
            // Your own constructor code
			if(($this->uri->segment(2) != 'ajax'))
			$this->load->view('header');
			
    }
	function index()
	{
		$data = array();
		 
		$page = 1;  
		$numrow =5;
		
		$start = ($page - 1) * $numrow; 
		
        $data['product'] = $this->Modelmembers->get_limit($numrow,$start);
		//print_r($data['product']);
		$total = $this->db->count_all('tbl_admin');
		$data['paging'] = $this->paging_ajax($total,$numrow,$page);
		
		$this->load->view('content/members/loaddata',$data);
	}
	 
	function addnew()
	{
		$data = array();
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('content/members/addnew');
		}
		else 
		{
			//Insert db
			 
        	$res = $this->Modelmembers->add_new();
			if($res == 1) 
			{
				$this->session->set_userdata('mess', 'Add new  sucessful');
				redirect('/members/', 'refresh'); 
			}
          
			else { $this->session->set_userdata('mess', 'Add new  false');
			 
			$this->load->view('content/members/addnew');}
		}
	}
	
	function ajax()
	{
		$view = $_POST['view'];
		switch($view)
		{
			case 'jump_page' : $this->jump_page(); break;
			case 'view_edit' : $this->view_edit(); break;
			case 'publish_obj' : $this->publish_obj(); break;
		}
		
		
	}
	function publish_obj()
	{
		 
		 //Publish
		 $res = $this->Modelmembers->publish();
		 if($res > 0) $this->jump_page();
	}
	function view_edit()
	{
		$data = array();
		$data['obj'] = $this->Modelmembers->get_item($_POST['idboj']);
		 
		if ($this->form_validation->run('members/add') == FALSE)
		{
			$this->load->view('content/members/editobj',$data);
		}
		else
		{
        	$res = $this->Modelmembers->update();
			 
			if($res == 1) 
				$this->session->set_userdata('mess', 'Edit sucessful');
          
			else $this->session->set_userdata('mess', 'Edit false');
			
			redirect('/members/', 'refresh'); 
			 
		}
		
	}
	function jump_page()
	{
		 
		$data = array();
	 
		$page = 1;  
		$numrow =5;
		if($_POST['page'] > 0) $page = $_POST['page'];
		
		$start = ($page - 1) * $numrow; 
		$data['product'] = $this->Modelmembers->get_limit($numrow,$start);
		$total = $this->db->count_all('tbl_admin');
		$data['paging'] = $this->paging_ajax($total,$numrow,$page);
         
		$this->load->view('content/members/load_ajax',$data);
	}
	 function paging_ajax($total,$num,$page)
	{
		
		$paging = '';
		$count_page = 0;
		if($total > 0)
		{
			$count_page = ceil($total / $num);
		}
			
		$paging = '<div class="rowBottom fixed">
                                
						  <div class="optionN">
							   
						  </div>
						  
						  <div class="pageNum">
							  <span class="marL10">Đến trang</span> <input type="text" id="page" name="page" value="'.$page.'" onkeyup="jumpPage(this.value)" class="inp inpNum">
							   
							  <span class="marC10">'.$page.' - '.$count_page.' của '.$count_page.'</span> <a onclick="jumpPage(\''.($page - 1) .'\')" class="bntPre"></a> <a onclick="jumpPage(\''.($page + 1) .'\')" class="bntNext"></a></a>
						  </div>
						  
					  </div>';
		//echo $paging; exit;			  
		return $paging;
	}
	
	public function email_check($email)
	{
		$obj = $this->Modelmembers->get_item_where($email);
		//print_r($obj);
		if (!empty($obj))
		{
			$this->form_validation->set_message('email_check', 'The %s field exits.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		redirect('/login/', 'refresh'); 
	}
}
