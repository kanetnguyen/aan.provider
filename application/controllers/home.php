<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
            parent::__construct();
           if($this->session->userdata('admin_id') < 1) redirect('/login/', 'refresh');  
			
    } 
	public function index()
	{
		$data = array();
		
		$this->load->view("content",$data);
		 
	}
	
	 
}
 