<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Livetv extends CI_Controller {
	private $data = array(); 
	public function __construct()
    {
        parent::__construct();
        // Your own constructor code
		if(($this->uri->segment(2) != 'ajax'))
		  $this->load->view('header');
    }
	function index()
	{
		$data=array();
        $id_parent_provider=$this->session->userdata('parent_id');  
        $row1=$this->Modellivetv->chech_id_provider($id_parent_provider);
        if(count($row1)>0)
        {
            $id_provider=$row1[0]['id'];
    		$page = 1;  
    		$numrow =NUM_ROWS;
    		//if($this->uri->segment(3) > 0) $page = $this->uri->segment(3);
    		
    		$start = ($page - 1) * $numrow; 
    
            $data['product'] = $this->Modellivetv->get_limit($numrow,$start,$id_provider);
            $total=$this->Modellivetv->sum($id_provider); 
        		
    		$this->load->library('Thuvien');
            $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page);
    		$data['id_provider']=$id_provider;    
        }
		$this->load->view('content/livetv/mytable_ajax',$data);
	}
    function error()
	{
		$data=array();
        $data['error']=1;
        $data['type']=1;
        $id_parent_provider=$this->session->userdata('parent_id');  
        $row1=$this->Modellivetv->chech_id_provider($id_parent_provider); // Lay ID Provider dua theo $id_parent_provider
        if(count($row1)>0)
        {
            $id_provider=$row1[0]['id'];
    		$page = 1;  
    		$numrow =NUM_ROWS;
    		//if($this->uri->segment(3) > 0) $page = $this->uri->segment(3);
    		$start = ($page - 1) * $numrow; 
            $data['product'] = $this->Modellivetv->get_limit_error($numrow,$start,$id_provider);
            $total=$this->Modellivetv->sumerror(1); 
        		
    		$this->load->library('Thuvien');
            $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page);
    		$data['id_provider']=$id_provider;    
        }
        
		$this->load->view('content/livetv/mytable_ajax',$data);
	}
	function ajax()
	{
		$view = $_POST['view'];
		switch($view)
		{
			case 'jump_page' : $this->jump_page($this->uri->segment(3),$this->uri->segment(4)); break;
			case 'view_edit' : $this->view_edit(); break;
			case 'publish_obj' : $this->publish_obj(); break;
            case 'search_search' : $this->search();break;
		}
	}
    function search()
    {
		$data = array();
         $id_provider=$_POST['value'];
        $search=$_POST['search'];
        $data['search']=$search;
        
		$page = 1;  
		$numrow =NUM_ROWS;
		//if($this->uri->segment(3) > 0) $page = $this->uri->segment(3);
		$start = ($page - 1) * $numrow; 
        
        if($search=="")
        {
            $data['product'] = $this->Modellivetv->get_limit($numrow,$start,$id_provider);
            $total=$this->Modellivetv->sum($id_provider); 
        } 
        else 
        {
            $data['product'] = $this->Modellivetv->search($numrow,$start,$id_provider,$search);
            $total=$this->Modellivetv->sumif($id_provider,$search);
        }
        
		$this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page);
		$data['id_provider']=$id_provider;
		$this->load->view('content/livetv/load_ajax',$data);
	}
    
	function publish_obj()
	{
		 //Publish
		 $res = $this->Modellivetv->publish();
		 if($res > 0) $this->jump_page();
	}
	function view_edit()
	{
		$data['obj'] = $this->Modellivetv->get_item($_POST['idboj']);
		$this->load->view('content/livetv/editobj',$data);		
	}
	function jump_page($id_provider,$search)
	{
        $data = array();
        $numrow = NUM_ROWS;
        $type=$_POST['type'];
        // Jump_page error
        
        if($type<9998)
        {   
            
            $total=$this->Modellivetv->sumerror($type); 
            $count_page=1; 
                if($total > 0)
        			$count_page = ceil($total / $numrow);
                $data = array();
        		$page = 1;  
        		// Lay Page
        		if($_POST['page'] > $count_page) $page = $count_page;
                else if ($_POST['page'] < 0) $page = 1;
                else  $page = $_POST['page'];
        		
        		$start = ($page - 1) * $numrow; 
                $data['product'] = $this->Modellivetv->get_limit_error($numrow,$start,$id_provider);
                $data['error']=1;
        }
        // Jump_page Dung chung
        else
        {
            if($search=="")
            {
                $total=$this->Modellivetv->sum($id_provider); 
                if($total > 0)
        			$count_page = ceil($total / $numrow);
                $data = array();
        		$page = 1;  
        		// Lay Page
        		if($_POST['page'] > $count_page) $page = $count_page;
                else if ($_POST['page'] < 0) $page = 1;
                else  $page = $_POST['page'];
        		
        		$start = ($page - 1) * $numrow; 
                $data['product'] = $this->Modellivetv->get_limit($numrow,$start,$id_provider);
            } 
            else 
            {
                $total=$this->Modellivetv->sumif($id_provider,$search);
                if($total > 0)
        			$count_page = ceil($total / $numrow);
                $data = array();
        		$page = 1;  
        		// Lay Page
        		if($_POST['page'] > $count_page) $page = $count_page;
                else if ($_POST['page'] < 0) $page = 1;
                else  $page = $_POST['page'];
        		
        		$start = ($page - 1) * $numrow; 
                $data['product'] = $this->Modellivetv->search($numrow,$start,$id_provider,$search);
                $data['search']=$search;
            }
            $data['id_provider']=$id_provider;
    		
        }
        
        $this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page);
        $this->load->view('content/livetv/load_ajax',$data);
        
	}

}
