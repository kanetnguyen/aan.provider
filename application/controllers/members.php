<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members extends CI_Controller {
	
	public function __construct()
    {
            parent::__construct();
            // Your own constructor code
			if(($this->uri->segment(2) != 'ajax'))
			$this->load->view('header');
    }
	function index()
	{
		$data = array();
		 
		$page = 1;  
		$numrow =NUM_ROWS;
		
		$start = ($page - 1) * $numrow; 
		
        // Lấy Bảng Dử Liệu theo Limit (Liên quan đến Database)
        $data['product'] = $this->Modelmembers->get_limit($numrow,$start);
		//print_r($data['product']);
		$sql = $this->Modelmembers->sum();
        $total=$sql[0]['count(*)'];

    	$this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
		
		$this->load->view('content/members/loaddata',$data);
	}
	 
	function addnew()
	{
		$data = array();
		
		if ($this->form_validation->run() == FALSE)
			$this->load->view('content/members/addnew');
		else 
		{
			//Insert db
        	$res = $this->Modelmembers->add_new();
			if($res == 1) 
			{
				$this->session->set_userdata('mess', 'Add new  sucessful');
				redirect('/members/', 'refresh'); 
			}
			else { $this->session->set_userdata('mess', 'Add new  false');
			$this->load->view('content/members/addnew');}
		}
	}
	
	function ajax()
	{
		$view = $_POST['view'];
		switch($view)
		{
			case 'jump_page' : $this->jump_page(); break;
			case 'view_edit' : $this->view_edit(); break;
			case 'publish_obj' : $this->publish_obj(); break;
		}	
	}
	function publish_obj()
	{
		 
		 //Publish
		 $res = $this->Modelmembers->publish();
		 if($res > 0) $this->jump_page();
	}
	
    function view_edit()
	{
		$data = array();
		$data['obj'] = $this->Modelmembers->get_item($_POST['idboj']);

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('content/members/view_edit',$data);
		}
		else
		{
        	$res = $this->Modelmembers->update();
			 
			if($res == 1) 
				$this->session->set_userdata('mess', 'Edit members sucessful');
          
			else $this->session->set_userdata('mess', 'Edit members false');
			
			redirect('/members/', 'refresh'); 	 
		}
	}
    function change_pass()
	{
		$data = array();
		//$data['obj'] = $this->Modelmembers->get_item($_POST['idboj']);
		 
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('content/members/changepass',$data);
		}
		else
		{
        		 
		}
	}
    
	function jump_page()
	{
	    $page = 1;  
		$numrow =NUM_ROWS;
        
        $sql = $this->Modelmembers->sum();
        $total=$sql[0]['count(*)'];
        
        if($total > 0)
		{
			$count_page = ceil($total / $numrow); // Lay so Nguyen Lam tron
		}
		$data = array();
	 
		
		if($_POST['page'] < 0) $page = 1;
        else if($_POST['page'] > $count_page) $page =$count_page;
        else $page=$_POST['page'];
		
		$start = ($page - 1) * $numrow; 
		$data['product'] = $this->Modelmembers->get_limit($numrow,$start);
		$this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
		$this->load->view('content/members/load_ajax',$data);
	}
    /**
     * public function email_check($email)
     * 	{
     *          return $this->commondb->email_check($email);
     * 	}
 */
	
	public function email_check($email)
	{
		$obj = $this->Modelmembers->get_item_where($email);
		//print_r($obj);
		if (!empty($obj))
		{
			$this->form_validation->set_message('email_check', 'The %s field exits.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		redirect('/login/', 'refresh'); 
	}
}
