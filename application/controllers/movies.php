<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movies extends CI_Controller {
    
	public function __construct()
    {
            parent::__construct();
            // Your own constructor code
			if(($this->uri->segment(2) != 'ajax'))
			$this->load->view('header');		
    }
	function index()
	{
		$id_parent_provider=$this->session->userdata('parent_id');  
        $row1=$this->Modelmovies->chech_id_provider($id_parent_provider);
        $data=array();
        if(count($row1)>0)
        {
            $id_provider=$row1[0]['id'];
    		$page = 1;  
    		$numrow =NUM_ROWS;
    		//if($this->uri->segment(3) > 0) $page = $this->uri->segment(3);
    		
    		$start = ($page - 1) * $numrow; 
    
            $data['product'] = $this->Modelmovies->get_limit($numrow,$start,$id_provider);
            $total=$this->Modelmovies->sum($id_provider); 
    		$this->load->library('Thuvien');
            $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
    		$data['id_provider']=$id_provider;    
        }        
            $this->load->view('content/movies/mytable_ajax',$data);
              
	}
    function chose()
    {
		$data = array();
        $id_provider=$_POST['value'];
        $search=$_POST['search'];
        
        $data['search']=$search;

		$page = 1;  
		$numrow =NUM_ROWS;
		//if($this->uri->segment(3) > 0) $page = $this->uri->segment(3);
		$start = ($page - 1) * $numrow; 
        
        if($search=="")
        {
            $data['product'] = $this->Modelmovies->get_limit($numrow,$start,$id_provider);
            $total=$this->Modelmovies->sum($id_provider); 
        } 
        else 
        {
            $data['product'] = $this->Modelmovies->search($numrow,$start,$id_provider,$search);
            $total=$this->Modelmovies->sumif($id_provider,$search);
        }
        //dfsdfsdfs
		$this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
		$data['id_provider']=$id_provider;
		$this->load->view('content/movies/load_ajax',$data);
	}
    function chose_error()
    {
		$data = array();
        $id_provider=$_POST['value'];
        $search=$_POST['search'];
        $data['error']=1;
        $data['type']=1;
        $data['search']=$search;
    
		$page = 1;  
		$numrow =NUM_ROWS;
		//if($this->uri->segment(3) > 0) $page = $this->uri->segment(3);
		$start = ($page - 1) * $numrow; 
        
        if($search=="")
        {
            $data['product'] = $this->Modelmovies->get_limit_error($numrow,$start,$id_provider);
            $total=$this->Modelmovies->sumerror(1,$id_provider); 
            $this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
        } 
        else 
        {
            $data['product'] = $this->Modelmovies->search_error($numrow,$start,$id_provider,$search);
            $total=$this->Modelmovies->sumerrorif(1,$id_provider,$search);
            $this->load->library('Thuvien');
            $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
        }

		$data['id_provider']=$id_provider;
        
		$this->load->view('content/movies/load_ajax',$data);
	}
    function error()
	{
		$data=array();
        $data['error']=1;
        $data['type']=1;
        $id_parent_provider=$this->session->userdata('parent_id');  
        $row1=$this->Modelmovies->chech_id_provider($id_parent_provider); // Lay ID Provider dua theo $id_parent_provider
        if(count($row1)>0)
        {
            $id_provider=$row1[0]['id'];

    		$page = 1;  
    		$numrow =NUM_ROWS;
    		//if($this->uri->segment(3) > 0) $page = $this->uri->segment(3);
    		$start = ($page - 1) * $numrow; 
            $data['product'] = $this->Modelmovies->get_limit_error($numrow,$start,$id_provider);
            
            $total=$this->Modelmovies->sumerror(1,$id_provider); 
            if($total<=0)
                $this->load->view('content/movies/mytable_ajax',$data);
            else
            {
                $this->load->library('Thuvien');
                $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page);
        		$data['id_provider']=$id_provider; 
            }	   
        }
        
		$this->load->view('content/movies/mytable_ajax',$data);
	}
	function add()
	{
		$data = array();
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('content/movies/addnew');
		}
		else
		{
        	$res = $this->Modelmovies->add_new();
			if($res == 1) 
				$this->session->set_userdata('mess', 'Add new provider sucessful');
          
			else $this->session->set_userdata('mess', 'Add new provider false');
			 
			$this->load->view('content/movies/addnew');
		}
	}
	
	function ajax()
	{
		$view = $_POST['view'];
		switch($view)
		{
			case 'jump_page' : $this->jump_page($this->uri->segment(3),$this->uri->segment(4)); break;
            case 'jump_page_error' : $this->jump_page_error($this->uri->segment(3),$this->uri->segment(4)); break;
			case 'view_edit' : $this->view_edit(); break;
			case 'publish_obj' : $this->publish_obj(); break;
            case 'choseee' : $this->chose();break;
            case 'chose_error' : $this->chose_error();break;
		}
	}
    
    function viewcao()
    {
        $data=array();
        $id_parent_provider=$this->session->userdata('parent_id');  
        $row1=$this->Modelmovies->chech_id_provider($id_parent_provider);
        if(count($row1)>0)
        {
            $id_provider=$row1[0]['id'];
    		$page = 1;  
    		$numrow =15;
    		//if($this->uri->segment(3) > 0) $page = $this->uri->segment(3);
    		$start = ($page - 1) * $numrow; 
            $data['product'] = $this->Modelmovies->view_cao($numrow,$start);    
        }
        
		$this->load->view('content/movies/mytable_ajax',$data);   
    }
	function publish_obj()
	{
		 $res = $this->Modelmovies->publish();
		 if($res > 0) $this->jump_page();
	}
	function view_edit()
	{
	   
		$data = array();
		$data['obj'] = $this->Modelmovies->get_item($_POST['idboj']);
		 
		$this->load->view('content/movies/editobj',$data);
	}
	function jump_page($id_provider,$str)
	{
		$numrow = NUM_ROWS;
        $data = array();
		$page = 1;
        $type=$_POST['type'];
        if($type<9998)
        {   
            if($str=="")
            {
                $total=$this->Modelmovies->sumerror($type,$id_provider); 
                $count_page=1; 
                if($total > 0)
        			$count_page = ceil($total / $numrow);
                $data = array();
        		$page = 1;  
        		// Lay Page
        		if($_POST['page'] > $count_page) $page = $count_page;
                else if ($_POST['page'] < 0) $page = 1;
                else  $page = $_POST['page'];
        		
        		$start = ($page - 1) * $numrow; 
                $data['product'] = $this->Modelmovies->get_limit_error($numrow,$start,$id_provider);
                $data['error']=1;
            }
            else
            {
                $total=$this->Modelmovies->sumerrorif($type,$id_provider,$str); 
                $count_page=1; 
                if($total > 0)
        			$count_page = ceil($total / $numrow);
                $data = array();
        		$page = 1;  
        		// Lay Page
        		if($_POST['page'] > $count_page) $page = $count_page;
                else if ($_POST['page'] < 0) $page = 1;
                else  $page = $_POST['page'];
        		
        		$start = ($page - 1) * $numrow; 
                $data['product'] = $this->Modelmovies->search_error($numrow,$start,$id_provider,$str);
                $data['error']=1;
            }
        }
        else
        {
                if($str=="")
                {
                    $total=$this->Modelmovies->sum($id_provider);
                    if($total > 0)
        			$count_page = ceil($total / $numrow);
            		// Lay Page
            		if($_POST['page'] > $count_page) $page = $count_page;
                    else if ($_POST['page'] < 0) $page = 1;
                    else  $page = $_POST['page'];
            		$start = ($page - 1) * $numrow; 
            		$data['product'] = $this->Modelmovies->get_limit($numrow,$start,$id_provider);
                }
                else
                {
                    $total=$this->Modelmovies->sumif($id_provider,$str);
                    if($total > 0)
        			$count_page = ceil($total / $numrow);
            		// Lay Page
            		if($_POST['page'] > $count_page) $page = $count_page;
                    else if ($_POST['page'] < 0) $page = 1;
                    else  $page = $_POST['page'];
            		$start = ($page - 1) * $numrow; 
            		$data['product'] = $this->Modelmovies->search($numrow,$start,$id_provider,$str);
                    $data['search']=$str;
                }
        }
		$this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 

        $data['id_provider']=$id_provider;
        
		$this->load->view('content/movies/load_ajax',$data);
	}

    
}
