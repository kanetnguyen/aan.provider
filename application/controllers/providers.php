<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Providers extends CI_Controller {
	
	public function __construct()
    {
            parent::__construct();
            // Your own constructor code
			if(($this->uri->segment(2) != 'ajax'))
			$this->load->view('header');
			
    }
	function index()
	{
		$data = array();
		 
		$page = 1;  
		$numrow =NUM_ROWS;
		if($this->uri->segment(3) > 0) $page = $this->uri->segment(3);
		
		$start = ($page - 1) * $numrow; 
		
        $data['product'] = $this->Modelproviders->get_limit($numrow,$start);
		
		$total = $this->db->count_all('tbl_parent_provider');
		$this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
		
		$this->load->view('content/providers/mytable_ajax',$data);
	}
	 
	function add()
	{
		$data = array();
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('content/providers/addnew');
		}
		else
		{
			//Insert db
        	$res = $this->Modelproviders->add_new();
			if($res == 1) 
				$this->session->set_userdata('mess', 'Add new provider sucessful');
			else $this->session->set_userdata('mess', 'Add new provider false');
			$this->load->view('content/providers/addnew');
		}
	}
	
	function ajax()
	{
		$view = $_POST['view'];
		switch($view)
		{
			case 'jump_page' : $this->jump_page(); break;
			case 'view_edit' : $this->view_edit(); break;
			case 'publish_obj' : $this->publish_obj(); break;
		}
		
		
	}
	function publish_obj()
	{
		 
		 //Publish
		 $res = $this->Modelproviders->publish();
		 if($res > 0) $this->jump_page();
	}
	function view_edit()
	{
		$data = array();
		$data['obj'] = $this->Modelproviders->get_item($_POST['idboj']);
		 
		if ($this->form_validation->run('providers/add') == FALSE)
		{
			$this->load->view('content/providers/editobj',$data);
		}
		else
		{
        	$res = $this->Modelproviders->update();
			 
			if($res == 1) 
				$this->session->set_userdata('mess', 'Edit provider sucessful');
          
			else $this->session->set_userdata('mess', 'Edit provider false');
			
			redirect('/providers/', 'refresh'); 
			 
		}
		
	}
	function jump_page()
	{
		 
		$page = 1;  
		$numrow =NUM_ROWS;
        $total = $this->db->count_all('tbl_parent_provider');
        if($total > 0)
		{
			$count_page = ceil($total / $numrow); // Lay so Nguyen Lam tron
		}
		$data = array();
	 
		
		if($_POST['page'] < 0) $page = 1;
        else if($_POST['page'] > $count_page) $page =$count_page;
        else $page=$_POST['page'];
		$start = ($page - 1) * $numrow; 
		$data['product'] = $this->Modelproviders->get_limit($numrow,$start);
		
		$this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
         
		$this->load->view('content/providers/load_ajax',$data);
	}
	
}
