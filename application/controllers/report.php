<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {
	
	public function __construct()
    {
            parent::__construct();
            // Your own constructor code
			if(($this->uri->segment(2) != 'ajax'))
			$this->load->view('header');
    }
	function index()
	{
		$data = array();
        $data['type']="balance";
        $this->load->view('content/report/loaddata',$data);
	}
	function thuebao()
	{
		$data = array();
        $data['loai']=0;
        $this->load->view('content/report/loaddata',$data);
	}
    function goi()
	{
		$data = array();
        $data['loai']=1;
        $this->load->view('content/report/loaddata',$data);
	}
	function export_excel()
	{
		$data = array();
		$data['title'] = "Báo cáo từ ngày 1/1/2013 đến ngày 12/4/2013";
		$data['child_title'] = array('ID','Name','Content');
		
		$data['product'] = $this->Modelreport->get_limit_balance($numrow=10,$start=1,"publish < 2 and vip = 0");
		 
		$data['merge'] = 'A1:C1';
		$data['size'] = 13;
		 
		$this->excel_report($data);
		 
	}
	
	function export_pdf()
	{
		$data = array();
		
		 
		$page = 1;  
		$numrow =NUM_ROWS;
		
		$start = ($page - 1) * $numrow; 
        // Lay du lieu theo Limit (Liên quan den Database)
        $data['product'] = $this->Modelreport->get_limit_balance($numrow,$start,"publish < 2 and vip = 0");
		//print_r($data['product']);
		$total = $this->Modelreport->sumif("publish < 2 and vip = 0");
        
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
		$data['balance'] = 2500 ;
		$data['promotion'] = 600 ;
		
		$html = $this->load->view('content/report/load_ajax',$data,TRUE);
		 
		 
		//$html = $this->load->view('content/mytable',$data,TRUE);
		
		 
	  	$this->load->library('MPDF56/mpdf');
	  	$this->mpdf=new mPDF('utf-8','A4','','',12,12,15,15,9,9);
		
	  	$this->mpdf->WriteHTML($html,2);
	  	$this->mpdf->Output('mpdf.pdf','I');
		 
		 
	}
	
	function excel_report($data)
	{
		//load our new PHPExcel library
		$this->load->library('excel');
		
		$objPHPExcel = new PHPExcel();
		
		 
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Accessasia TV")
									 ->setLastModifiedBy("Accessasia TV")
									 ->setTitle("Office 2007 XLSX Detail Report")
									 ->setSubject("Office 2007 XLSX Detail Report")
									 ->setDescription("Detail Report for Office 2007 XLSX, generated using PHP classes.")
									 ->setKeywords("office 2007 openxml php")
									 ->setCategory("Report");
		
		$objPHPExcel->setActiveSheetIndex(0);
		//name the worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Report');
		//set cell A1 content with some text
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $data['title']);
		//change the font size
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(17);
		//make the font become bold
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//merge cell A1 until D1
		$objPHPExcel->getActiveSheet()->mergeCells($data['merge']);
		//set aligment to center for that merged cell (A1 to D1)
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		 //set title
		 
		 $objPHPExcel->setActiveSheetIndex(0);
		 $kt = 'A';
		 foreach($data['child_title'] as $value)
		 {
			$objPHPExcel->getActiveSheet()->getStyle($kt.'2')->getFont()->setSize($data['size']);
			$objPHPExcel->getActiveSheet()->getStyle($kt.'2')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValue($kt.'2', $value);
			$kt ++;
		 }
		
		
		$flag = 2;
		//csprint_r($data['product']);exit;
		foreach($data['product'] as $row)
		{ 
			
			$flag ++;
			// Add some data
			$objPHPExcel->getActiveSheet()->getStyle('A'.$flag)->getFont()->setSize($data['size']);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$flag)->getFont()->setBold(true);
			// array('Name', 'Color', 'Size'), id, title, intro
			$kt = 'A';
			$objPHPExcel->setActiveSheetIndex(0);
			foreach($row as $value)
			{
				$objPHPExcel->getActiveSheet()->setCellValue($kt.$flag, $value);
				$kt ++;
			}
			 
			
			}
		 
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		$filename='detail_report.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel = $objPHPExcel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
 
		
	}
	
	
	function ajax()
	{
		$view = $_POST['view'];
 
		switch($view)
		{
			case 'jump_page' : $this->jump_page($this->uri->segment(3)); break;
			case 'view_edit' : $this->view_edit(); break;
			case 'publish_obj' : $this->publish_obj(); break;
			
			case 'tracuu' : $this->xl_tracuu(); break;
			case 'detail' : $this->xl_detail(); break;
			case 'export' :
				if($_POST['file'] == 'pdf') $this->export_pdf(); 
				else $this->export_excel(); break;
		}	
	}
	
	function xl_tracuu()
	{
	   $loai=$_POST['loai'];
		$date_to = $_POST['date_to'];
		$date_from = $_POST['date_from'];
        $this->Modelreport->tracuu( $date_to,$date_from);
        if($loai=="a")
        {
            $data['balance'] = $this->Modelreport->sum_balance($date_to,$date_from);
		  $data['promotion'] = $this->Modelreport->sum_promotion($date_to,$date_from);
        }
        else
        {
            $data['balance'] = $this->Modelreport->sum_balance($date_to,$date_from,$loai);
		  $data['promotion'] = $this->Modelreport->sum_promotion($date_to,$date_from,$loai);
        }
		
		$this->load->view('content/report/load_balance',$data); 
	}
	function xl_detail()
	{
        $loai=$_POST['loai'];
		$type = $_POST['type'];
		$date_to = $_POST['date_to'];
		$date_from = $_POST['date_from'];
        $data['type']=$type;
		$page = 1;  
		$numrow =NUM_ROWS;
		
		$start = ($page - 1) * $numrow; 
        // Lay du lieu theo Limit (Liên quan den Database)
        if($type=="balance")
        {
            if($loai=="a")
            {
                $data['product'] = $this->Modelreport->get_limit_balance($numrow,$start,$date_to,$date_from);
                $total = $this->Modelreport->count_balance($date_to,$date_from);    
            }  
            else
            {
                $data['product'] = $this->Modelreport->get_limit_balance($numrow,$start,$date_to,$date_from,$loai);
                $total = $this->Modelreport->count_balance($date_to,$date_from,$loai);    
            } 
            
        }  
        if($type=="promotion")
        {
            if($loai=="a")
            {
                $data['product'] = $this->Modelreport->get_limit_promotion($numrow,$start,$date_to,$date_from);
                $total = $this->Modelreport->count_promotion($date_to,$date_from);
            }
            else
            {
                $data['product'] = $this->Modelreport->get_limit_promotion($numrow,$start,$date_to,$date_from,$loai);
                $total = $this->Modelreport->count_promotion($date_to,$date_from,$loai);
            }
        }
        $this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
        if($loai==1)
		        $data['title'] = "Thuê Bao [ ".$type . "] (".$date_to." to ".$date_from.")";
        else if($loai==0)
                $data['title'] = "Bán Lẽ [ ".$type . "] (".$date_to." to ".$date_from.")";
        else
                $data['title'] = "Doanh Thu [ ".$type . "] (".$date_to." to ".$date_from.")";
		$this->load->view('content/report/load_ajax',$data);
	}

	function publish_obj()
	{
		 
		 //Publish
		 $res = $this->Modelreport->publish();
		 if($res > 0) $this->jump_page("");
	}
	function view_edit()
	{
        $data = array();
		$data['obj'] = $this->Modelreport->get_view($_POST['idboj']);
		 
		$this->load->view('content/report/view_edit',$data);
		}

    
	function jump_page($type)
	{
	    $loai=$_POST['loai'];
	    $page = 1;  
		$numrow =NUM_ROWS;
        $date_to=$_POST['date_to'];
        $date_from=$_POST['date_from'];
        $data['type']=$type;
        
        if ($type=="balance")
        {
            if($loai=="a")
                $total = $this->Modelreport->count_balance($date_to,$date_from);
            else
                $total = $this->Modelreport->count_balance($date_to,$date_from,$loai);
            if($total > 0)
    			$count_page = ceil($total / $numrow); // Lay so Nguyen Lam tron
    		$data = array();
    		if($_POST['page'] < 0) $page = 1;
            else if($_POST['page'] > $count_page) $page =$count_page;
            else $page=$_POST['page'];
    		$start = ($page - 1) * $numrow; 
            if($loai=="a")
    		  $data['product'] = $this->Modelreport->get_limit_balance($numrow,$start,$date_to,$date_from);
            else
                $data['product'] = $this->Modelreport->get_limit_balance($numrow,$start,$date_to,$date_from,$loai);
        }
        if($type=="promotion")
        {
            if($loai=="a")
                $total = $this->Modelreport->count_promotion($date_to,$date_from);
            else
                $total = $this->Modelreport->count_promotion($date_to,$date_from,$loai);
            if($total > 0)
    			$count_page = ceil($total / $numrow); // Lay so Nguyen Lam tron
    		$data = array();
    		if($_POST['page'] < 0) $page = 1;
            else if($_POST['page'] > $count_page) $page =$count_page;
            else $page=$_POST['page'];
    		$start = ($page - 1) * $numrow; 
            if($loai=="a")
    		      $data['product'] = $this->Modelreport->get_limit_promotion($numrow,$start,$date_to,$date_from);
            else
                $data['product'] = $this->Modelreport->get_limit_promotion($numrow,$start,$date_to,$date_from,$loai);
        }
        if($loai==1)
		        $data['title'] = "Thuê Bao [ ".$type . "] (".$date_to." to ".$date_from.")";
        else if($loai==0)
                $data['title'] = "Bán Lẽ [ ".$type . "] (".$date_to." to ".$date_from.")";
        else
                $data['title'] = "Doanh Thu [ ".$type . "] (".$date_to." to ".$date_from.")";
        $this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
		$this->load->view('content/report/load_ajax',$data);
	}

	
	public function email_check($email)
	{
		$obj = $this->Modelreport->get_item_where($email);
		//print_r($obj);
		if (!empty($obj))
		{
			$this->form_validation->set_message('email_check', 'The %s field exits.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		redirect('/login/', 'refresh'); 
	}
}
