<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usersnomal extends CI_Controller {
	
	public function __construct()
    {
            parent::__construct();
            // Your own constructor code
			if(($this->uri->segment(2) != 'ajax'))
			$this->load->view('header');
    }
	function index()
	{
		$data = array();
		 
		$page = 1;  
		$numrow =NUM_ROWS;
		
		$start = ($page - 1) * $numrow; 
		
        
                // L?y B?ng D? Li?u theo Limit (Liên quan d?n Database)
        $data['product'] = $this->Modelusers->get_limit($numrow,$start,"publish < 2 and vip = 0");
		//print_r($data['product']);
		$total = $this->Modelusers->sumif("publish < 2 and vip = 0");
        $this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
		$this->load->view('content/users/loaddata',$data);
	}
	function chose()
    {
		$data = array();
        $search=$_POST['search'];
        
		$page = 1;  
		$numrow =NUM_ROWS;
		//if($this->uri->segment(3) > 0) $page = $this->uri->segment(3);
		$start = ($page - 1) * $numrow; 
        $str="publish < 2 and vip = 0 AND `email` LIKE '%".$search."%' ";
            $data['product'] = $this->Modelusers->search($numrow,$start,$str);
            $total=$this->Modelusers->sumif($str);
		$this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
		$this->load->view('content/users/load_ajax',$data);
	}
	
	function ajax()
	{
		$view = $_POST['view'];
		switch($view)
		{
			case 'jump_page' : $this->jump_page($this->uri->segment(3)); break;
			case 'view_edit' : $this->view_edit(); break;
			case 'publish_obj' : $this->publish_obj(); break;
            case 'choseee' : $this->chose();break;
		}	
	}

	function publish_obj()
	{
		 
		 //Publish
		 $res = $this->Modelusers->publish();
		 if($res > 0) $this->jump_page("");
	}
	function view_edit()
	{
        $numrow=5;
		$data = array();
        $data['type']=$_POST['type'];
        if(isset($_POST['page']))
            $data['page']=$_POST['page'];  
        else
            $data['page']=1;
        
            
        switch($data['type'])
        {
            case 1:
                $data['total']=count($this->Modelusers->get_chitiet($_POST['idboj']));
                break;
            case 2:
                $data['total']=count($this->Modelusers->get_login($_POST['idboj']));
                break;
            case 3:
                $data['total']=count($this->Modelusers->get_buy($_POST['idboj']));
                break;
            case 4:
                $data['total']=count($this->Modelusers->get_naptien($_POST['idboj']));
                break;
        } 
        $data['idopj']=$_POST['idboj'];
        $total=$data['total'];
        $page_tamp=$data['page'];
        
        if($total>0)
        {
            $count_page = ceil($total / $numrow);
            if($page_tamp < 0) $page = 1;
            else if($page_tamp > $count_page) $page =$count_page;
            else $page=$page_tamp;
            $start = ($page - 1) * $numrow;
        }
        else
        {
            $start = 0;
            $page=1;
        }
        
        $data['page']=$page;
        $data['numrow']=$numrow;
        switch($data['type'])
        {
            case 1:
                $data['obj'] = $this->Modelusers->get_chitiet($_POST['idboj']);
                break;
            case 2:
                $data['obj'] = $this->Modelusers->get_login($_POST['idboj'],$numrow,$start);
                break;
            case 3:
                $data['obj'] = $this->Modelusers->get_buy($_POST['idboj'],$numrow,$start);
                break;
            case 4:
                $data['obj'] = $this->Modelusers->get_naptien($_POST['idboj'],$numrow,$start);
                break;
        }
		$this->load->view('content/users/view_edit',$data);
		}

    
	function jump_page($search)
	{
	    $page = 1;  
		$numrow =NUM_ROWS;
        if ($search=="")
        {
            $total = $this->Modelusers->sumif("publish < 2 and vip = 0");
            if($total > 0)
    			$count_page = ceil($total / $numrow); // Lay so Nguyen Lam tron
    		$data = array();
    		if($_POST['page'] < 0) $page = 1;
            else if($_POST['page'] > $count_page) $page =$count_page;
            else $page=$_POST['page'];
    		$start = ($page - 1) * $numrow; 
    		$data['product'] = $this->Modelusers->get_limit($numrow,$start,"publish < 2 and vip = 0");
        }
        else
        {
            $str="publish < 2 and vip = 0 AND `email` LIKE '%".$search."%' ";
            $total=$this->Modelusers->sumif($str);
            if($total > 0)
    			$count_page = ceil($total / $numrow); // Lay so Nguyen Lam tron
    		$data = array();
    		if($_POST['page'] < 0) $page = 1;
            else if($_POST['page'] > $count_page) $page =$count_page;
            else $page=$_POST['page'];
    		$start = ($page - 1) * $numrow; 
    		$data['product'] = $this->Modelusers->get_limit($numrow,$start,$str);
        }
        $this->load->library('Thuvien');
        $data['paging'] =$this->thuvien->paging_ajax($total,$numrow,$page); 
		$this->load->view('content/users/load_ajax',$data);
	}

	
	public function email_check($email)
	{
		$obj = $this->Modelusers->get_item_where($email);
		//print_r($obj);
		if (!empty($obj))
		{
			$this->form_validation->set_message('email_check', 'The %s field exits.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		redirect('/login/', 'refresh'); 
	}
}
