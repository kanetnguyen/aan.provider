<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common {
	
	/**
     * function decrypt for int  
     */
    public function decrypt($x, $key = PRIVATE_KEY) { // decrypt    
        $x .= $key; $s = '';
        foreach (explode("\n", trim(chunk_split($x, 2))) as $h)
            $s .= chr(hexdec($h));
		$num = substr($s, 0, -5); 
		return($num);
		 
    }
	 
	
}

/* End of file Someclass.php */