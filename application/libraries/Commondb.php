<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commondb {

    public function permission_provider($level_provider,$name_module)
    {  
		//1: aan; 2:vnpt-g(providers have access to the highest powers, = admin aan); 3: provider normal
		$arr_block_module = array();
		if($level_provider == 2 && $name_module == 'providers') return 0;
		elseif($level_provider == 3 && ($name_module == 'providers' || $name_module == 'users')) return 0;
		return 1;
    }
    public function email_check($email)
	{ 
		$obj = $this->Modelusers->get_item_where($email);
		//print_r($obj);
		if (!empty($obj))
		{
			$this->form_validation->set_message('email_check', 'The %s field exits.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
}

/* End of file Someclass.php */