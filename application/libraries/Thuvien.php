<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Thuvien {
	
	function paging_ajax($total,$num,$page,$idopj="",$type="")
	{
		$paging = '';
		$count_page = 0;
		if($total > 0)
			$count_page = ceil($total / $num); // Lay so Nguyen Lam tron
        
        if($idopj!="" && $type!="")
        {
            if($page<=1)
                $paging = '<div class="rowBottom fixed">
    						  <div class="optionN"></div>
    						  <div class="pageNum">
    							  <span class="marL10">Đến trang</span> <input type="text" id="page" name="page" value="'.$page.'" onkeyup="jumpPageAjax(this.value)" class="inp inpNum">
    							  <span class="marC10">'.$page.' - '.$count_page.' của '.$count_page.'</span>  <a onclick="jumpPageAjax('.($page + 1).','.$idopj.','.$type.')" class="bntNext"></a></a>
    						  </div></div>';
            else if($page>=$count_page)
                $paging = '<div class="rowBottom fixed">
    						  <div class="optionN"></div>
    						  <div class="pageNum">
    							  <span class="marL10">Đến trang</span> <input type="text" id="page" name="page" value="'.$page.'" onkeyup="jumpPageAjax(this.value)" class="inp inpNum">
    							  <span class="marC10">'.$page.' - '.$count_page.' của '.$count_page.'</span> <a onclick="jumpPageAjax('.($page - 1).','.$idopj.','.$type.')" class="bntPre"></a> </a>
    						  </div></div>';
    		else 
                $paging = '<div class="rowBottom fixed">
    						  <div class="optionN"></div>
    						  <div class="pageNum">
    							  <span class="marL10">Đến trang</span> <input type="text" id="page" name="page" value="'.$page.'" onkeyup="jumpPageAjax(this.value)" class="inp inpNum">
    							  <span class="marC10">'.$page.' - '.$count_page.' của '.$count_page.'</span> <a onclick="jumpPageAjax('.($page - 1).','.$idopj.','.$type.')" class="bntPre"></a> <a onclick="jumpPageAjax('.($page + 1).','.$idopj.','.$type.')" class="bntNext"></a></a>
    						  </div></div>';
        }
        else
        {
            if($page<=1)
                $paging = '<div class="rowBottom fixed">
    						  <div class="optionN"></div>
    						  <div class="pageNum">
    							  <span class="marL10">Đến trang</span> <input type="text" id="page" name="page" value="'.$page.'" onkeyup="jumpPage(this.value)" class="inp inpNum">
    							  <span class="marC10">'.$page.' - '.$count_page.' của '.$count_page.'</span>  <a onclick="jumpPage(\''.($page + 1) .'\')" class="bntNext"></a></a>
    						  </div></div>';
            else if($page>=$count_page)
                $paging = '<div class="rowBottom fixed">
    						  <div class="optionN"></div>
    						  <div class="pageNum">
    							  <span class="marL10">Đến trang</span> <input type="text" id="page" name="page" value="'.$page.'" onkeyup="jumpPage(this.value)" class="inp inpNum">
    							  <span class="marC10">'.$page.' - '.$count_page.' của '.$count_page.'</span> <a onclick="jumpPage(\''.($page - 1) .'\')" class="bntPre"></a> </a>
    						  </div></div>';
    		else  
                $paging = '<div class="rowBottom fixed">
    						  <div class="optionN"></div>
    						  <div class="pageNum">
    							  <span class="marL10">Đến trang</span> <input type="text" id="page" name="page" value="'.$page.'" onkeyup="jumpPage(this.value)" class="inp inpNum">
    							  <span class="marC10">'.$page.' - '.$count_page.' của '.$count_page.'</span> <a onclick="jumpPage(\''.($page - 1) .'\')" class="bntPre"></a> <a onclick="jumpPage(\''.($page + 1) .'\')" class="bntNext"></a></a>
    						  </div></div>';
        }

            
		//echo $paging; exit;			  
		return $paging;
	}

    
}

/* End of file Someclass.php */