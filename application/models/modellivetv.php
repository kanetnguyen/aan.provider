<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modellivetv extends CI_Model { 

    function __construct()
    {
        parent::__construct();
    }
	function add_new()
	{
		$data = array(
		   'name' => $_POST['name'] ,
		   'level' => $_POST['type']  
		);
		$res = $this->db->insert('tbl_livetv', $data); 
		return $res;
	}
	function get_limit($numrow,$start,$idpro)
	{
        $this->db->select("tv.id id, name, publish_second, image");
        $this->db->from('tbl_livetv tv');
        $this->db->join("tbl_livetv_lang l","l.livetv_id = tv.id");
        $this->db->where("id_provider",$idpro);
        $this->db->where("l.lang_id",2);
        $this->db->order_by('name asc');
        $this->db->limit($numrow,$start);
        $query=$this->db->get();
		return $query->result();
	}
    function get_limit_error($numrow,$start,$idpro)
	{
	   $this->db->select('e.id id,e.content content, e.publish publish, l.name name, ,m.email email, e.date date'); // Chua xong
        $this->db->limit($numrow,$start);
        $this->db->where('e.type',2);
        $this->db->where('l.lang_id',2);
        $this->db->join('tbl_members m','m.id=e.member_id');
        $this->db->join('tbl_livetv_lang  l','l.livetv_id=e.obj_id');  
        $query=$this->db->get('tbl_error_link e');
		return $query->result();
	}
    
    function search($numrow,$start,$idpro,$str)
	{
        $query=$this->db->query("SELECT `tbl_livetv`.`id` AS `id` , `name` , `publish_second` , `image`, `view`
                                FROM `tbl_livetv` , `tbl_livetv_lang`
                                WHERE `tbl_livetv`.`id` = `tbl_livetv_lang`.`livetv_id`
                                AND `id_provider` ='$idpro'
                                AND `tbl_livetv_lang`.`lang_id` ='2'
                                AND `name` LIKE '%$str%'
                                order by `name` asc
                                LIMIT $start , $numrow
        ");
		return $query->result();
	}
	
	
	function get_item($id)
	{
		$query=$this->db->query("SELECT `tbl_livetv`.`id` AS `id` , `name` , `publish_second` , `image`, `view`,`intro_text`,`intro_text_app`
                                FROM `tbl_livetv` , `tbl_livetv_lang`
                                WHERE `tbl_livetv`.`id` = `tbl_livetv_lang`.`livetv_id`
                                AND `tbl_livetv_lang`.`lang_id` ='2'
                                AND `tbl_livetv`.`id`='$id'
                                
        ");
		return $query->result();
	}
	
	function update()
	{
		$id = $_POST['idboj'];
		$data = array(
		   'name' => $_POST['name'] ,
		   'level' => $_POST['type']
		    
		);
		 
		$res = $this->db->update('tbl_film', $data, "id = $id ");
		return $res;
	}
	
	function publish()
	{
		$id = $_POST['id_obj'];
		$data = array(
		   'publish' => $_POST['value'] 
		    
		);
		//$this->db->where('id', $id);
		//$res = $this->db->update('tbl_film', $data);
		$res = $this->db->update('tbl_film', $data, "id = $id ");
		return $res;
	}
    function sum($idpro)
    {
        $query=$this->db->query("SELECT count(*)
                                FROM `tbl_livetv` , `tbl_livetv_lang`
                                WHERE `tbl_livetv`.`id` = `tbl_livetv_lang`.`livetv_id`
                                AND `id_provider` =$idpro
                                AND `tbl_livetv_lang`.`lang_id` ='2' ");
		$sql=$query->result_array();
        return $sql[0]['count(*)'];
    }
    function sumif($idpro,$str)
    {
        $sql= $this->db->query("SELECT COUNT(*) 
                                FROM `tbl_livetv` , `tbl_livetv_lang`
                                WHERE `tbl_livetv`.`id` = `tbl_livetv_lang`.`livetv_id`
                                AND `id_provider` LIKE  '%$idpro%'
                                AND `lang_id` ='2'
                                AND `name` LIKE '%$str%'
                                ");
        $sql1=$sql->result_array();
        return $sql1[0]['COUNT(*)'];
    }
    //Xet trong table error 
    //Truy?n v�o ki? type   1:VOD  2: LIvetv
    function sumerror($type)
    {
        $this->db->select('COUNT(*)');
        $this->db->where('type',$type);
        $sql=$this->db->get('tbl_error_link');
        $sql1=$sql->result_array();
        return $sql1[0]['COUNT(*)'];
    }
	function name_provider($id_provider)
    {
        $sql=$this->db->query("SELECT `name` FROM `tbl_provider` WHERE `id`='$id_provider'");
        $sql1=$sql->result_array();
        return $sql1[0]['name'];
    }
    function chech_id_provider($id_parent_provider)
    {
        if($id_parent_provider!=1)
            $this->db->where('id_parent_provider',$id_parent_provider);
        $this->db->where('type',1);
        $Parent_Provider = $this->db->get('tbl_provider');
        return $Parent_Provider->result_array();
    }
	 
}
