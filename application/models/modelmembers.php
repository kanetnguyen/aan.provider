<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelmembers extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	function add_new()
	{
	   
		$data = array(
		   'fullname' => $_POST['name'] ,
		   'email' => $_POST['email'] ,
		   'password' =>  md5(sha1(trim($_POST['password']))) ,
		   'parent_id' => $this->session->userdata('parent_id') ,
		   'type' => 2 ,
		   'publish' => 1
		    
		);
		// In so dong Insert Thanh cong
		$res = $this->db->insert('tbl_admin', $data); 
		return $res;
	}
	function get_limit($numrow,$start)
	{
		$this->db->order_by("id", "desc");
        $pr_id=$this->session->userdata('parent_id');  
		$this->db->where('parent_id',$pr_id);
		$this->db->where('publish < 2');
		$query = $this->db->get('tbl_admin', $numrow,$start);
		return $query->result();
	}
	
	function get_item($id)
	{
		$this->db->select('id, fullname, email, password');
		$this->db->where('id', $id);
		$query = $this->db->get('tbl_admin');
        return $query->row_object(); 
		//return $data;
	}
	
	function get_item_where($email)
	{
		//$this->db->select('id, name, level');
		$this->db->where('email', $email);
		$query = $this->db->get('tbl_admin');
		return $query->row_object(); 
		//return $data;
	}
	
	function update()
	{
		$id = $_POST['idboj'];
		$data = array(
		   'password' =>  md5(sha1(trim($_POST['password']))) );
		$res = $this->db->update('tbl_admin', $data, "id = $id ");
		return $res;
	}
	function publish()
	{
		$id = $_POST['id_obj'];
		$data = array(
		   'publish' => $_POST['value'] 
		);
		//$this->db->where('id', $id);
		//$res = $this->db->update('tbl_admin', $data);
		$res = $this->db->update('tbl_admin', $data, "id = $id ");
		return $res;
	}
    function sum()
    {
        $this->db->select('count(*)');
        $this->db->where('publish < 2');
        $pr_id=$this->session->userdata('parent_id');  
		$this->db->where('parent_id',$pr_id);
		$query = $this->db->get('tbl_admin');
		return $query->result_array();
    }
	
	 
}
