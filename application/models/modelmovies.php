<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelmovies extends CI_Model {

    function __construct()
    {
        parent::__construct();

    }
	function add_new()
	{
		$data = array(
		   'name' => $_POST['name'] ,
		   'level' => $_POST['type'] );
		$res = $this->db->insert('tbl_film', $data); 
		return $res;
	}
	function get_limit($numrow,$start,$idpro)
	{
		$this->db->limit($numrow,$start);
		$this->db->order_by("f.id", "desc");
		$this->db->select('f.id id, name, publish,image,view,total_episode,director');
		$this->db->where('lang_id = 1');
        $where = 'FIND_IN_SET("'.$idpro.'", id_provider) ';
		$this->db->where($where);
		$this->db->from('tbl_film f');
		$this->db->join('tbl_film_lang  l', 'f.id = l.film_id');
		
		$query = $this->db->get();
 		//SELECT `f`.`id` id, `name`, `publish`, `image`, `view`, `total_episode`, `director` FROM (`tbl_film` f) JOIN `tbl_film_lang` l ON `f`.`id` = `l`.`film_id` WHERE `lang_id` = 1 AND (2) In (`id_provider`) ORDER BY `f`.`id` desc LIMIT 5
		
        //$query=$this->db->query("SELECT `tbl_film`.`id` as `id`, `name`, `publish`,`image`,`view`,`total_episode`,`director`
        //                    FROM `tbl_film` , `tbl_film_lang`
        //                    WHERE `tbl_film`.`id` = `tbl_film_lang`.`film_id`
        //                   AND `lang_id` =1
        //                    AND `id_provider` LIKE '%$idpro%'
        //                  LIMIT $start , $numrow");
		return $query->result();
	}
    function get_limit_error($numrow,$start,$idpro)
	{
	   $this->db->select('e.id id,e.content content, e.publish publish, l.name name, ,m.email email, e.date date, f.image image'); // Chua xong
        $this->db->limit($numrow,$start);
        $this->db->where('e.type',1);
        $this->db->where('l.lang_id',2);
        $where = 'FIND_IN_SET("'.$idpro.'", `f`.`id_provider`) ';
        $this->db->where($where);
        $this->db->join('tbl_members m','m.id=e.member_id');
        $this->db->join('tbl_film_lang  l','l.film_id=e.obj_id'); 
        $this->db->join('tbl_film  f','f.id=e.obj_id'); 
        $query=$this->db->get('tbl_error_link e');
		return $query->result();
	}
    function view_cao($numrow,$start)
	{
		$this->db->limit($numrow,$start);
		$this->db->order_by("view", "desc");
        
		$this->db->select('f.id id, name, publish,image,view,total_episode,director');
		$this->db->where('lang_id = 1');
		$this->db->from('tbl_film f');
		$this->db->join('tbl_film_lang  l', 'f.id = l.film_id');
		$query = $this->db->get();
 		//SELECT `f`.`id` id, `name`, `publish`, `image`, `view`, `total_episode`, `director` FROM (`tbl_film` f) JOIN `tbl_film_lang` l ON `f`.`id` = `l`.`film_id` WHERE `lang_id` = 1 AND (2) In (`id_provider`) ORDER BY `f`.`id` desc LIMIT 5
		
        //$query=$this->db->query("SELECT `tbl_film`.`id` as `id`, `name`, `publish`,`image`,`view`,`total_episode`,`director`
        //                    FROM `tbl_film` , `tbl_film_lang`
        //                    WHERE `tbl_film`.`id` = `tbl_film_lang`.`film_id`
        //                   AND `lang_id` =1
        //                    AND `id_provider` LIKE '%$idpro%'
        //                  LIMIT $start , $numrow");
		return $query->result();
	}
    
    function search($numrow,$start,$idpro,$str)
	{
		$this->db->limit($numrow,$start);
		$this->db->order_by("f.id", "desc");
		$this->db->select('f.id id, name, publish,image,view,total_episode,director');
		$this->db->where('lang_id = 1');
        $where = 'FIND_IN_SET("'.$idpro.'", id_provider) ';
		$this->db->where($where);
		$this->db->like('name', $str);
		$this->db->from('tbl_film f');
		$this->db->join('tbl_film_lang  l', 'f.id = l.film_id');	
		$query = $this->db->get();
		return $query->result();
	}
    function search_error($numrow,$start,$idpro,$str)
	{
		$this->db->select('e.id id,e.content content, e.publish publish, l.name name, ,m.email email, e.date date, f.image image'); // Chua xong
        $this->db->limit($numrow,$start);
        $this->db->where('e.type',1);
        $this->db->where('l.lang_id',2);
        $where = 'FIND_IN_SET("'.$idpro.'", `f`.`id_provider`) ';
        $this->db->where($where);
        $this->db->where("l.name LIKE '%$str%'");
        $this->db->join('tbl_members m','m.id=e.member_id');
        $this->db->join('tbl_film_lang  l','l.film_id=e.obj_id'); 
        $this->db->join('tbl_film  f','f.id=e.obj_id'); 
        $query=$this->db->get('tbl_error_link e');
		return $query->result();
	}
	
	
	function get_item($id)
	{
        //$query=$this->db->query("SELECT `tbl_film`.`id` as `id`, `name`, `publish`,`image`,`total_episode`,`intro_text`,`full_text`
//                            FROM `tbl_film` , `tbl_film_lang`
//                            WHERE `tbl_film`.`id` = `tbl_film_lang`.`film_id` 
//                            AND `tbl_film`.`id`= $id
//                            AND `lang_id` =1
//                            ");return $query->result(); 
		$this->db->select('f.id id, name, publish,view,total_episode,director,image,intro_text,full_text');
		$this->db->where('f.id', $id);
		$this->db->where('lang_id', 1);
		$this->db->from('tbl_film f');
		$this->db->join('tbl_film_lang  l', 'f.id = l.film_id');
		$query = $this->db->get();
		return $query->row_object(); 
		//return $data;
	}
	
	function update()
	{
		$id = $_POST['idboj'];
		$data = array(
		   'name' => $_POST['name'] ,
		   'level' => $_POST['type']  
		);
		$res = $this->db->update('tbl_film', $data, "id = $id ");
		return $res;
	}
	
	function publish()
	{
		$id = $_POST['id_obj'];
		$data = array(
		   'publish' => $_POST['value'] 
		    
		);
		//$this->db->where('id', $id);
		//$res = $this->db->update('tbl_film', $data);
		$res = $this->db->update('tbl_film', $data, "id = $id ");
		return $res;
	}
    //Xet trong table error 
    //Truyền vào kiể type   1:VOD  2: LIvetv
    function sumerror($type,$idpro)
    {
        $this->db->select('COUNT(*)');
        $this->db->where('e.type',$type);
        $where = 'FIND_IN_SET("'.$idpro.'", `f`.`id_provider`) ';
		$this->db->where($where);
        $this->db->join('tbl_members m','m.id=e.member_id');
        $this->db->join('tbl_film  f','f.id=e.obj_id'); 
        $query=$this->db->get('tbl_error_link e');
		$sql1=$query->result_array();
        return $sql1[0]['COUNT(*)'];
    }
    function sum($so)
    {
       // $sql= $this->db->query("SELECT COUNT( * ) FROM  `tbl_film` WHERE  `id_provider` LIKE  '%$so%'");
//        $sql1=$sql->result_array();
//        return $sql1[0]['COUNT( * )'];
		
		$where = 'FIND_IN_SET("'.$so.'", id_provider) ';
		$this->db->where($where);
		$this->db->from('tbl_film');
		return $this->db->count_all_results();
		
    }
	/**
     * $so 
     * $str (vd 'abc')
     * Trả về số lượng dòng kiểu int
     *  FontVod::ListVod($where = 'and hot = 1 ', $limit = 'Limit 0, 6')
     */
    function sumif($so,$str)
    {
		$where = 'FIND_IN_SET("'.$so.'", id_provider) '; 
		$this->db->where($where);
		$this->db->where('lang_id', 1);
		$this->db->like('name',$str);
		$this->db->from('tbl_film f');
		$this->db->join('tbl_film_lang  l', 'f.id = l.film_id'); 
		return $this->db->count_all_results();	
    }
    function sumerrorif($type,$idpro,$str)
    {
        $this->db->select('COUNT(*)');
        $this->db->where('e.type',$type);
        $this->db->where('l.lang_id',2);
        $where = 'FIND_IN_SET("'.$idpro.'", `f`.`id_provider`) ';
		$this->db->where($where);
        $this->db->like('`l`.`name`',$str);
        $this->db->join('tbl_members m','m.id=e.member_id');
        $this->db->join('tbl_film_lang  l','l.film_id=e.obj_id'); 
        $this->db->join('tbl_film  f','f.id=e.obj_id'); 
        $query=$this->db->get('tbl_error_link e');
		$sql1=$query->result_array();
        return $sql1[0]['COUNT(*)'];
    }
    function count_episodes($id)
    {
		 
		$this->db->where('film_id',$id);
		$this->db->from('tbl_film_episodes');
		return $this->db->count_all_results();
		
        //$sql= $this->db->query("SELECT COUNT( * ) FROM  `tbl_film_episodes` WHERE  `film_id`=$id");
//        $sql1=$sql->result_array();
//        return $sql1[0]['COUNT( * )'];
    }
	function name_provider($id_provider)
    {
		$this->db->select('name');
		$this->db->where('id', $id_provider);
		 
		$this->db->from('tbl_provider');
		$query = $this->db->get();
		$obj = $query->row_object(); 
		return $obj->name;
        //$sql=$this->db->query("SELECT `name` FROM `tbl_provider` WHERE `id`=$id_provider");
//        $sql1=$sql->result_array();
//        return $sql1[0]['name'];
    }
    function chech_id_provider($id_parent_provider)
    {
        $this->db->where('id_parent_provider',$id_parent_provider);
        $this->db->where("`type` <> 1");
        $Parent_Provider = $this->db->get('tbl_provider');
        return $Parent_Provider->result_array();
    }
	 
}
