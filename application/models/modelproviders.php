<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelproviders extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	function add_new()
	{
		$data = array(
		   'name' => $_POST['name'] ,
		   'level' => $_POST['type']
		    
		);
		
		$res = $this->db->insert('tbl_parent_provider', $data); 
		return $res;
	}
	function get_limit($numrow,$start)
	{
		 
		$this->db->where('publish < 2');
		$query = $this->db->get('tbl_parent_provider', $numrow,$start);
		return $query->result();
	}
	
	
	function get_item($id)
	{
		$this->db->select('id, name, level');
		$this->db->where('id', $id);
		$query = $this->db->get('tbl_parent_provider');
		return $query->row_object(); 
		//return $data;
	}
	
	function update()
	{
		$id = $_POST['idboj'];
		$data = array(
		   'name' => $_POST['name'] ,
		   'level' => $_POST['type']
		    
		);
		 
		$res = $this->db->update('tbl_parent_provider', $data, "id = $id ");
		return $res;
	}
	
	function publish()
	{
		$id = $_POST['id_obj'];
		$data = array(
		   'publish' => $_POST['value'] 
		    
		);
		//$this->db->where('id', $id);
		//$res = $this->db->update('tbl_parent_provider', $data);
		$res = $this->db->update('tbl_parent_provider', $data, "id = $id ");
		return $res;
	}
	
	 
}
