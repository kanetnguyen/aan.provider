<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelreport extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function get_limit_promotion($numrow,$start,$fdate,$tdate,$type="")
	{
	   if($type!="")
            $this->db->where("type",$type);
	    $this->db->select("id,member_id,id_package,id_provider,money,free_balance,b.date date");
	    $this->db->where("`b`.`date` >= ",$fdate);
        $this->db->where("`b`.`date` <= ",$tdate);
        $this->db->where("b.buy_from <> 1");
        $this->db->where("b.buy_from <> 2");
        $this->db->where("b.buy_from <> 3");
        $this->db->from("tbl_log_buyfilm b");
        $this->db->limit($numrow,$start);
        $query = $this->db->get();
        return $query->result();
	}
 	function get_limit_balance($numrow,$start,$fdate,$tdate,$type="")
	{
	   if($type!="")
            $this->db->where("type",$type);
	    $this->db->select("id,member_id,id_package,id_provider,money,free_balance,b.date date");
	    $this->db->where("`b`.`date` >= ",$fdate);
        $this->db->where("`b`.`date` <= ",$tdate);
        $this->db->where("b.buy_from <> 0");
        $this->db->where("b.buy_from <> 4");
        $this->db->from("tbl_log_buyfilm b");
        $this->db->limit($numrow,$start);
        $query = $this->db->get();
        return $query->result();
	}
    function get_email_member($id)
    {
        $this->db->select('email');
        $this->db->where('id',$id);  
        $query = $this->db->get("tbl_members");
        $kq=$query->result_array();
		return $kq[0]['email'];
    }
    function get_package($id)
    {
        $this->db->select('name');
        $this->db->where('id',$id);  
        $query = $this->db->get("tbl_package");
        $kq=$query->result_array();
        if(count($kq)>0)
		  return $kq[0]['name'];
        else
            return "";
    }
	function get_provider($id)
    {
        $this->db->select('name,logo');
        $this->db->where('id',$id);  
        $query = $this->db->get("tbl_provider");
        return $query->result_array();
    }
    function get_view($id)
	{
	   $this->db->select("id,member_id,id_package,id_provider,money,free_balance,b.date date");
	    $this->db->where("id",$id);
        $this->db->from("tbl_log_buyfilm b");
        $query = $this->db->get();
        return $query->result_array();
	}
	function get_item_where($email)
	{
		//$this->db->select('id, name, level');
		$this->db->where('email', $email);
		$query = $this->db->get('tbl_admin');
		return $query->row_object(); 
		//return $data;
	}

	function publish()
	{
		$id = $_POST['id_obj'];
		$data = array('publish' => $_POST['value']);
        
		//$this->db->where('id', $id);
		//$res = $this->db->update('tbl_admin', $data);
		$res = $this->db->update('tbl_members', $data, "id = $id");
		return $res;
	}

    function tracuu($fdate, $tdate)
    {
        $this->db->where("`date` >= ",$fdate);
        $this->db->where("`date` <= ",$tdate);
        $this->db->from("tbl_log_buyfilm");
        $query = $this->db->get();
        return $query->result();
    }
    // Sum Tien ao
    // Type 0,4
    function sum_promotion($fdate, $tdate, $type="")
    {
        if($type!="")
            $this->db->where("type",$type);
        $this->db->select(" SUM(`free_balance`) ");
        $this->db->where("`date` >= ",$fdate);
        $this->db->where("`date` <= ",$tdate);
        $this->db->where("buy_from <> 1");
        $this->db->where("buy_from <> 2");
        $this->db->where("buy_from <> 3");
        $this->db->from("tbl_log_buyfilm");
        $query = $this->db->get();
        $kq=$query->result_array();
		return $kq[0]['SUM(`free_balance`)'];
    }
    // Sum Tien that
    // Type 1,2,3
    function sum_balance($fdate, $tdate, $type="")
    {
        if($type!="")
            $this->db->where("type",$type);
        $this->db->select(" SUM(`money`) ");
        $this->db->where("`date` >= ",$fdate);
        $this->db->where("`date` <= ",$tdate);
        $this->db->where("buy_from <> 0");
        $this->db->where("buy_from <> 4");
        $this->db->from("tbl_log_buyfilm");
        $query = $this->db->get();
        $kq=$query->result_array();
		return $kq[0]['SUM(`money`)'];
    }
    function count_promotion($fdate, $tdate, $type="")
    {
        if($type!="")
            $this->db->where("type",$type);
        $this->db->select("count(*)");
        $this->db->where("`date` >= ",$fdate);
        $this->db->where("`date` <= ",$tdate);
        $this->db->where("buy_from <> 1");
        $this->db->where("buy_from <> 2");
        $this->db->where("buy_from <> 3");
        $this->db->from("tbl_log_buyfilm");
        $query = $this->db->get();
        $kq=$query->result_array();
		return $kq[0]['count(*)'];
    }
    // Sum Tien that
    // Type 1,2,3
    function count_balance($fdate, $tdate, $type="")
    {
        if($type!="")
            $this->db->where("type",$type);
        $this->db->select("count(*)");
        $this->db->where("`date` >= ",$fdate);
        $this->db->where("`date` <= ",$tdate);
        $this->db->where("buy_from <> 0");
        $this->db->where("buy_from <> 4");
        $this->db->from("tbl_log_buyfilm");
        $query = $this->db->get();
        $kq=$query->result_array();
		return $kq[0]['count(*)'];
    }
    
	
	 
}
