<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelusers extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function get_limit($numrow,$start,$str="")
	{
	   $this->db->select('m.id id , publish , email, last_login, balance, last_update, vip, phone,free_balance ');
        $this->db->from('tbl_members m');
        $this->db->join('tbl_balance b','m.id = b.members_id');
        $this->db->order_by("email", "asc"); 
        if($str!="")
            $this->db->where($str);
        $this->db->limit($numrow,$start);
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_chitiet($id)
	{
	    $this->db->select('m.id id,email,fullname,vip,last_login,phone, p.id_provider pro');
		$this->db->from('tbl_members m');
        $this->db->join('tbl_balance b','m.id = b.members_id'); 
        $this->db->join('tbl_package_duration p','m.id = p.member_id'); 
        $this->db->where('m.id',$id);
        $this->db->where('p.publish','1');
        $this->db->order_by("m.id", "asc"); 
		$query = $this->db->get();
        return $query->result_array();
	}
	function get_buy($id,$numrow=0,$start=0)
	{
	    $this->db->select('b.id id , b.date date, b.expired_date expired_date, pro.name name,p.name loainame, p.image, p.price');
		$this->db->from('tbl_log_buyfilm b');
        $this->db->join('tbl_package p','p.id = b.id_package'); 
        $this->db->join('tbl_provider pro','pro.id = p.id_provider'); 
        $this->db->where('b.member_id',$id);
        if($numrow!=0 || $start!=0)  
            $this->db->limit($numrow,$start);
        $this->db->order_by("b.id", "desc"); 
		$query = $this->db->get();
        return $query->result_array();
	}
    function get_login($id,$numrow=0,$start=0)
	{
	    $this->db->select('m.id id , email,fullname,date_login, date_logout,ip');
		$this->db->from('tbl_members m');
        $this->db->join('tbl_log_login l','m.id = l.member_id'); 
        $this->db->where('m.id',$id);
        $this->db->order_by("date_login", "desc");
        if($numrow!=0 || $start!=0)  
            $this->db->limit($numrow,$start);
		$query = $this->db->get();
        return $query->result_array();
	}
    public function get_name_provider($id_provider)
    {
        $this->db->select('name');
        $this->db->where('id',$id_provider);
        $sql=$this->db->get('tbl_provider');
      
        return $sql->row_object();
    }
    function get_naptien($id,$numrow=0,$start=0)
	{
	   $this->db->select('id , date,amount,charge_from,description');
       $this->db->where('member_id',$id);
       if($numrow!=0 || $start!=0)  
            $this->db->limit($numrow,$start);
		$query = $this->db->get('tbl_log_charge');
        return $query->result_array();
	}
	function get_item_where($email)
	{
		//$this->db->select('id, name, level');
		$this->db->where('email', $email);
		$query = $this->db->get('tbl_admin');
		return $query->row_object(); 
		//return $data;
	}
    // CHua xong
	function search($numrow,$start,$str)
	{
        $this->db->from('tbl_members m');
        $this->db->join('tbl_balance b','m.id = b.members_id');
        $this->db->order_by("email", "asc"); 
        $this->db->where($str);
        $this->db->limit($numrow,$start);
		$query = $this->db->get();
		return $query->result();
	}
	function update()
	{
		$id = $_POST['idboj'];
		$data = array(
            'fullname' =>  $_POST['name'],
            'phone' =>  $_POST['phone'],
            'vip' =>  $_POST['RadioGroup']
        );
		$res = $this->db->update('tbl_members', $data, "id = $id ");
		return $res;
	}
	function publish()
	{
		$id = $_POST['id_obj'];
		$data = array('publish' => $_POST['value']);
        
		//$this->db->where('id', $id);
		//$res = $this->db->update('tbl_admin', $data);
		$res = $this->db->update('tbl_members', $data, "id = $id");
		return $res;
	}
    function sumall()
    {
        $this->db->select('count(*)');
        $this->db->from('tbl_members m');
        $this->db->join('tbl_balance b','m.id = b.members_id');
		$query = $this->db->get();
        $kq=$query->result_array();
		return $kq[0]['count(*)'];
    }
    function sumif($where)
    {
        $this->db->select('count(*)');
        $this->db->from('tbl_members m');
        $this->db->join('tbl_balance b','m.id = b.members_id');
        $this->db->where($where);  
        $query = $this->db->get();
        $kq=$query->result_array();
		return $kq[0]['count(*)'];
    }

	
	 
}
