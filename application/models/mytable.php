<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mytable extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	function add_new()
	{
		$data = array(
		   'title' => $_POST['name'] ,
		   'intro' => $_POST['intro']
		    
		);
		
		$res = $this->db->insert('tbl_product', $data); 
		return $res;
	}
	
	function get_item()
	{
		$this->db->select('id, title, intro');
		$query = $this->db->get('tbl_product');
		return $query->result(); 
		//return $data;
	}
	
	function update($id)
	{
		$data = array(
		   'title' => $_POST['name'] ,
		   'intro' => $_POST['intro']
		    
		);
		$this->db->where('id', $id);
		$res = $this->db->update('tbl_product', $data);
		//or $this->db->update('mytable', $data, "id = 4");
		return $res;
	}
	
	function delete()
	{
		$res = $this->db->delete('mytable', array('id' => $id)); 
		return $res;
	}
	
	function delete_multi()
	{
		$tables = array('table1', 'table2', 'table3');
		$this->db->where('id', '5');
		$this->db->delete($tables);
		return $res;
	}
	
	function get_all()
	{
		$rows = $this->db->get('tbl_product');
		return $rows;
	}
	 
	function get_limit($start,$end)
	{
		$query = $this->db->get('tbl_product');
		return $query->result();
	}
	function get_where()
	{
		// $this->db->where('name', $name);
		// $this->db->where('title', $title);
		// $this->db->where('status', $status);
		// == WHERE name = 'Joe' AND title = 'boss' AND status = 'active' 
		
		// $this->db->where('name !=', $name);
		// $this->db->where('id <', $id);
		// == Produces: WHERE name != 'Joe' AND id < 45 

		// $array = array('name' => $name, 'title' => $title, 'status' => $status);
		// or $array = array('name !=' => $name, 'id <' => $id, 'date >' => $date);
		// or $where = "name='Joe' AND status='boss' OR status='active'"; --> $this->db->where($where);
		// $this->db->like('title', 'match');
		// $this->db->like('body', 'match');
		// == WHERE title LIKE '%match%' AND body LIKE '%match%
		
		// $this->db->where($array); 

		// $this->db->where('name !=', $name);
		// $this->db->or_where('id >', $id);
		// == Produces: WHERE name != 'Joe' OR id > 50

		$rows = $this->db->get_where('tbl_product', array('id' => $id), $limit, $offset);
		return $rows;
	}
	
	function get_sum()
	{
		$this->db->select('(SELECT SUM(payments.amount) FROM payments WHERE payments.invoice_id=4) AS amount_paid', FALSE);
		$query = $this->db->get('mytable');
		//$this->db->select_sum('age');
		//$query = $this->db->get('members');
		// Produces: SELECT SUM(age) as age FROM members
	}
}
