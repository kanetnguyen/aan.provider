<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:::: CodeInterger ::::</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/init.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/library.js"></script>


<script type="text/javascript" src="scripts/tytabs.jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#tabsholder").tytabs({
			fadespeed:"fast"
							});
});
</script>

<script type="text/javascript" src="scripts/vscontext.jquery.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
            $('#context1').vscontext({menuBlock: 'vs-menu1'});
			$('#context2').vscontext({menuBlock: 'vs-menu2'});
	});
</script>

<link rel="stylesheet" href="css/reveal.css">	
<script type="text/javascript" src="scripts/jquery.reveal.js"></script>

</head>

<body>

	<div class="siteAd siteLog">
    	
        <!-- header -->
        <div class="headerAd">
        	<a href="#" class="logo"><img src="images/logo.png" /></a>
            <div class="blogUser"> 
            	Hello <b><?php echo($this->session->userdata('username')) ;  ?></b> <a href="members/logout" class="icon icoLogout" title="Logout">Logout</a> |  <a href="members/changepass" class="icon icoChangepass"  data-reveal-id="blogMatKhau" title="Change password">Change password</a>
                
                <!-- light box -->
                <div id="blogMatKhau" class="reveal-modal boxGhiChu blogFrom">
                    <a class="close-reveal-modal"></a>
                    <div class="boxIn">
                        <h4 class="titleBox">Đổi mật khẩu</h4> 
                        <div class="contentBox boxForm">
                        	<label>Mật khẩu cũ</label><input type="password" class="inp inpLog" value="<?php echo($this->session->userdata('password')) ;  ?>"/><br />
                            <label>Mật khẩu mới</label><input type="text" class="inp inpLog" /><br />
                            <label>Gõ lại mật khẩu mới</label><input type="text" class="inp inpLog" /><br />
                            <label> </label><input type="submit" value="Đồng ý" class="bntAll" />
                        </div>
                    </div>
                </div>
                <!-- en light box -->
                
            </div>
        </div>
        <!-- en header -->
        
        <!-- nav -->
        <div class="navTop">
        	<!--<ul>
            	<li><a href="#"><span>Dashboard</span></a></li>
                <li><a href="#" class="active"><span>Quản lý tin bài</span></a></li>
                <li><a href="#"><span>Quản lý comment</span></a></li>
                <li><a href="#"><span>Quản lý chủ đề</span></a></li>
                <li><a href="#"><span>Thống kê</span></a></li>
                <li><a href="#"><span>Bình chọn</span></a></li>
            </ul>-->
            <div class="clr"></div>
        </div>
        <!-- en nav -->
        
        <!-- bodyMain -->
        <div class="pageMain">
        	
            <div class="pageHome">
                
                <!-- content -->
        
                <div class="blogContentDB">
                	
                  	<ul class="listDB">
                        <?php  if($this->session->userdata('level_provider') <= 3 ) { ?>
                    	<li class="m1"><a href="movies"><span>Manager Movies</span></a></li>
                        <?php } if($this->session->userdata('level_provider') < 3 ) { ?>
                        <li class="m2"><a href="livetv"><span>Manager TV</span></a></li>
                        <?php } ?>
                         
                        <li class="m5"><a href="members"><span>My User</span></a></li>
                        <?php if($this->session->userdata('level_provider') < 3 ) { ?>
                        <li class="m5"><a href="users"><span>Members AAN</span></a></li>
                        <?php } ?>
                        <li class="m6"><a href="report"><span>Reports</span></a></li>
                        <?php if($this->session->userdata('level_provider') == 1) { ?>
                        <li class="m7"><a href="providers"><span>Providers</span></a></li>
                        <?php } ?>
                        <li class="m10"><a href="mailbox"><span>MailBox</span></a></li>
                    </ul>
                  
                   
                </div>
                <!-- en content -->
            
           
            <div class="clr"></div>
        	
            </div>
            	
        </div>
        <!-- en bodyMain -->   
        
        <!-- footer -->
        <div class="footerAdIn">Copyright 2012 - 2013 <span>AAN</span>. Allright services</div>
        <!-- en footer --> 
        
    </div>

</body>
</html>
