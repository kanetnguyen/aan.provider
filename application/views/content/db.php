<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 <!-- bodyMain -->
        <div class="pageMain">
        	
            <!-- main left -->
            <?php $view_left = $this->uri->segment(1); $this->load->view('left/'.$view_left); ?>
            <!-- en main left -->
            
            <!-- main right -->
            <div class="mainRight">
            	
               
                <!-- content -->
                
                <div class="blogContent">
                	
                    <div class="rowHeader rowHeader2 fixed">
                    	<i class="icon iconTaomoi"></i> Add DB
                    </div>
                    
                    <div class="boxContent">
                    	<?php echo form_open('myform/db'); ?>
                        
                        <!-- box tieu de -->
                        <div class="boxFill">
                        	<a href="#" class="icon minF"></a>
                        	<h4 class="title">Textbox</h4>
                            <div class="contentFill formFill contT">
                                 
                                <label>Title (không rỗng và < 6 kí tự) <span class="red">(*)</span></label>
                                <?php echo form_error('name', '<div class="error">', '</div>'); //Gan div cho cụ thể báo lỗi nào ?>
                        		<input type="text" name="name" value="<?php echo set_value('name'); ?>" class="inp inpTitle"  />
                                <br />
                                <label>Intro <span class="red">(Phần tóm tắt không quá 150 từ)</span></label>
                                <?php echo form_error('intro', '<div class="error">', '</div>'); //Gan div cho cụ thể báo lỗi nào ?>
                                <textarea name="intro" class="area areaSort"></textarea>
                            </div>  
                        </div>
                        <!-- en box tieu de -->
                    
                        <div class="boxFill">
                        	<a href="#" class="icon minF"></a>
                        	<h4 class="title">Textbox</h4>
                            <div class="contentFill formFill contT">
                                <label>Select box</label>
                                 <select name="myselect" class="sel selN">
                                    <option value="one" <?php echo set_select('myselect', 'one'); ?> >One 1</option>
                                    <option value="two" <?php echo set_select('myselect', 'two', TRUE); ?> >Two 2</option>
                                    <option value="three" <?php echo set_select('myselect', 'three'); ?> >Three 3</option>
                                </select> 
                                 <br />
                                <label>Check box</label>
                                 <input type="checkbox" name="mycheck[]" value="1" <?php echo set_checkbox('mycheck[]', '1'); ?> />
                        		<input type="checkbox" name="mycheck[]" value="2" <?php echo set_checkbox('mycheck[]', '2',TRUE); ?> />
                                <br />
                                <label>Radio</label>
                                Nam <input type="radio" name="myradio" value="1" <?php echo set_radio('myradio', '1', TRUE); ?> />
                        		Nu <input type="radio" name="myradio" value="2" <?php echo set_radio('myradio', '2'); ?> />
                            </div>  
                        </div>
                        
                        
                        <!-- box anh -->
                        <div class="boxFill" style="display:none">
                        	<a href="#" class="icon minF"></a>
                        	<h4 class="title">Ảnh bài viết</h4>
                            <div class="contentFill formFill contT">
                            	<label>Chọn ảnh</label>
                                <input type="file" size="70" class="inp inpTitle" /><br />
                                <label>Chọn icon</label>
                                <input type="file"  size="70" class="inp inpTitle" /><br />
                            </div>
                        </div>
                        <!-- en box anh -->
                         
                        
                        <div class="bntBottom">
                        	<input type="submit" value="Submit" class="bntAll"  />
                        	 
                        </div>
                        
                        <div class="padT10"></div>
                       
                    </div>
                
                </div>
                 
                <!-- en content -->
            
            </div>
            <!-- en main right -->
            
            <div class="clr"></div>
        
        </div>
        <!-- en bodyMain -->   
        
        <!-- footer -->
        <div class="footerAdIn">Copyright 2012 - 2013 <span>AAN</span>. Allright services</div>
        <!-- en footer --> 
        
    </div>

</body>
</html>
<script type="text/javascript">
CreateEditor('content', 960, 250);

</script>