<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 <!-- bodyMain -->
        <div class="pageMain">
        	
            <!-- main left -->
            <?php $view_left = $this->uri->segment(1); $this->load->view('left/'.$view_left); ?>
            <!-- en main left -->
            
            <!-- main right -->
            <div class="mainRight">
            	
               
                <!-- content -->
                
                <div class="blogContent">
                	
                    <div class="rowHeader rowHeader2 fixed">
                    	<i class="icon iconTaomoi"></i> Form Upload img
                    </div>
                    
                    <div class="boxContent">
                    	<?php echo @$error;?>
						<?php echo form_open_multipart('myform/do_upload');?>

                        <!-- box anh -->
                        <div class="boxFill">
                        	<a href="#" class="icon minF"></a>
                        	<h4 class="title">Hình ảnh</h4>
                            <div class="contentFill formFill contT">
                            	<label>Chọn ảnh</label>
                                <input type="file" size="70" name="userfile" class="inp inpTitle" /><br />
                                <label>Tên ảnh</label>
                                <input type="text"  size="70" class="inp inpTitle" /><br />
                            </div>
                        </div>
                        <!-- en box anh -->
                         
                        
                        <div class="bntBottom">
                        	<input type="submit" value="upload" class="bntAll"  />
                        	 
                        </div>
                        
                        <div class="padT10"></div>
                       
                    </div>
                
                </div>
                 
                <!-- en content -->
            
            </div>
            <!-- en main right -->
            
            <div class="clr"></div>
        
        </div>
        <!-- en bodyMain -->   
        
        <!-- footer -->
        <div class="footerAdIn">Copyright 2012 - 2013 <span>AAN</span>. Allright services</div>
        <!-- en footer --> 
        
    </div>

</body>
</html>