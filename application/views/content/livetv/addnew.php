<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 <!-- bodyMain -->
        <div class="pageMain">
        	
            <!-- main left -->
            <?php $view_left = $this->uri->segment(1); $this->load->view('left/'.$view_left); ?>
            <!-- en main left -->
            
            <!-- main right -->
            <div class="mainRight">
            	
               
                <!-- content -->
                
                <div class="blogContent">
                	
                    <div class="rowHeader rowHeader2 fixed">
                    	<i class="icon iconTaomoi"></i> Add new Provider
                    </div>
                    
                    <div class="boxContent">
                    	 
					<?php echo $this->session->userdata('mess') ; $this->session->unset_userdata('mess'); ?>
						<?php 
						$attributes = array('id' => 'myform');
						echo form_open('providers/add',$attributes); 
						?>
                        
                        <!-- box tieu de -->
                        <div class="boxFill">
                        	<a href="#" class="icon minF"></a>
                        	<h4 class="title">Information</h4>
                            <div class="contentFill formFill contT">
                                 
                                <label>Name <span class="red">(*)</span></label>
                                <?php echo form_error('name', '<div class="error_ci">', '</div>'); //Gan div cho cụ thể báo lỗi nào ?>
                        		<input type="text" name="name" id="name" value="<?php echo set_value('name'); ?>" class="inp inpTitle"  />
                                <br />
                                
                                <label>Type <span class="red">(*)</span></label>
                                
                        		<select name="type" id="type" class="sel selN">
                                    <option value="1" <?php echo set_select('myselect', '1'); ?> >AAN 2</option>
                                    <option value="2" <?php echo set_select('myselect', '2', TRUE); ?> >vnpt-g(providers have access to the highest powers)</option>
                                    <option value="3" <?php echo set_select('myselect', '3'); ?> >Provider normal</option>
                                </select> 
                                <br />
                                
                            </div>  
                        </div>
                        <!-- en box tieu de -->
                          
                        <div class="bntBottom">
                        	<input type="submit" value="Submit" class="bntAll"  />
                        	 
                        </div>
                        
                        <div class="padT10"></div>
                       
                    </div>
                
                </div>
                 
                <!-- en content -->
            
            </div>
            <!-- en main right -->
            
            <div class="clr"></div>
        
        </div>
        <!-- en bodyMain -->   
        
        <!-- footer -->
        <div class="footerAdIn">Copyright 2012 - 2013 <span>AAN</span>. Allright services</div>
        <!-- en footer --> 
        
    </div>

</body>
</html>
<script type="text/javascript">

$(function(){
   $('#myform').validate({
        rules: {
            
            'name': { required: true }
        },
        errorElement: "div",
		errorPlacement: function(error, element) { 
			offset = element.offset();    
			error.insertBefore(element);  
			error.css({'position': 'absolute', 'left': offset.left + element.outerWidth() - 100, 'top': offset.top - $('.error').height() - 20 });
		},
        messages: {
           
            'name': { required: 'Please enter name.' }   
        }
   });
   
    
});
</script>