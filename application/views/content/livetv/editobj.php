<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="boxContent">
                    	 
<?php echo $this->session->userdata('mess') ; $this->session->unset_userdata('mess'); ?>
	<?php 
	$attributes = array('id' => 'myform');
	echo form_open('movies/view_edit',$attributes); 
    
    ?>
	 
             
	<!-- box tieu de -->
	<div class="boxFill">
		<a href="#" class="icon minF"></a>
		<h4 class="title">Info <?php echo $obj[0]->name;?></h4>
		<div class="contentFill formFill contT">
			 
			<label>Image <span class="red">(*)</span></label>
            <?php $str= "<img height='75px' src='".IMG_FILM.$obj[0]->image."' />"; echo $str;?>
            
			<input type="hidden" name="idboj" id="idboj" value="<?php echo $obj[0]->id;?>" />
			<br />
			<label>Tình Trạng : <span class="red"><?php if($obj[0]->publish_second == 1) echo "Đang Hiện !! "; else echo "Đã Ẫn !!";?></span></label> 
            <label>Lượt Xem : <span class="red"><?php echo $obj[0]->view; ?></span></label> 
            <label>Giới Thiệu : <span class="red"><?php echo $obj[0]->intro_text; ?></span></label> 
            <label>Nội Dung : <span class="red"><?php echo $obj[0]->intro_text_app; ?></span></label> 

            
			<br />
			
		</div>  
	</div>
	<!-- en box tieu de -->
	  
	<!-- <div class="bntBottom"><input type="submit" value="Submit" class="bntAll"  /></div> -->
	
	<div class="padT10"></div>
   
</div>
<script type="text/javascript">

$(function(){
   $('#myform').validate({
        rules: {
            
            'name': { required: true }
        },
        errorElement: "div",
		errorPlacement: function(error, element) { 
			offset = element.offset();    
			error.insertBefore(element);  
			error.css({'position': 'absolute', 'left': offset.left + element.outerWidth() - 300, 'top': offset.top - $('.error').height() - 90 });
		},
        messages: {
           
            'name': { required: 'Please enter name.' }   
        }
   });
   
    
});
</script>