<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 <!-- bodyMain -->
        <div class="pageMain"> 
        	
            <!-- main left -->
            <?php if(isset($product))
            {
                $view_left = $this->uri->segment(1); $this->load->view('left/'.$view_left); }?>
            <!-- en main left -->
            <!-- main right -->
            <div class="mainRight">
            	<div class="blogSearch"><!-- search -->
                <div id="tabsholder">
                <?php
                if(!isset($error))
                {
                    if(isset($id_provider)) 
                    {
                    ?>
                    <ul class="tabs"><li id="tab1" >Tìm kiếm</li></ul>
                    <div class="boxAll">             
                        <div id="content1" class="tabscontent"> 
                              <?php 
                                    $id_parent_provider=$this->session->userdata('parent_id');  
                                    $row1=$this->Modellivetv->chech_id_provider($id_parent_provider);
                                   
                                  ?>
                                  <form name="a" method="post" action="livetv">
                                  <?php
                                  if( count($row1) > 1 ) 
                                  {
                                    ?>
                                    <select  id="id_provider" class="sel selN2"  >
                                     <?php  foreach($row1 as $row) 
                                            {
                                                  $id = $row['id'];									 														
                                                  $name = $row['name'];							   	  
                                                  ?><option value="<?php echo $id;?>"><?php echo $name;?></option><?php 
                                            }
                                           ?> 
                                    </select>
                                    </form>
                                       <br />   
                                       <input type="text" name="search" id="search" value="<?php echo set_value('search'); ?>" class="inp inpTitle"  />
                                    <span class="bntAll" onclick="search();">Tìm kiếm</span>
                                    
                                    <?php
                                  }
                                  else
                                  {
                                   
                                    ?>
                                    <input type="hidden"  id="value_value" value="<?php echo $row1[0]['id'];?>" />
                                    </form>
                                       <br />   
                                       <input type="text" name="search" id="search" value="<?php echo set_value('search'); ?>" class="inp inpTitle"  />
                                    <span class="bntAll" onclick="search();">Tìm kiếm</span>
                                    <?php
                                  }                  
                              ?>                        
                        </div>
                    </div>
                    <?php 
                    }
                    }
                    else
                        $data['error']=$error;
                    ?>     
                </div>
            </div>
               
                <!-- content -->
                    <input type="hidden"  id="type" value="<?php echo (isset($type))?$type:10000;?>" />
                
                    <div id="idList">
                    <?php echo $this->session->userdata('mess') ; $this->session->unset_userdata('mess'); 
                            if(isset($product))
                            {
                                $data['product'] = $product;
            					$data['paging'] = $paging;
                                $data['id_provider']=$id_provider;
            					$this->load->view('content/livetv/load_ajax',$data);
                            }
                            else
                                echo "Không Có Dữ Liệu";
					?>
                    
                	</div>
                
                 
                <!-- en content -->
            
            </div>
            <!-- en main right -->
            
            <div class="clr"></div>
        
        </div>
        <!-- en bodyMain -->   
        
        <!-- footer -->
        <div class="footerAdIn">Copyright 2012 - 2013 <span>AAN</span>. Allright services</div>
        <!-- en footer --> 
        
    </div>

 <!-- light box -->
<div id="blogChitiet" class="reveal-modal boxChiTiet">
    <a class="close-reveal-modal"></a>
    <div class="boxIn contentBI">
        <h4 class="titleBox">View</h4> 
        <div id="idDetail">
        	<div class="contentBox">Nội dung</div>
        </div>
    </div>
</div>
<!-- en light box -->
</body>
</html>
<script>
function jumpPage(page)
{   
    if($('#id_provider').length)
        var value = $('#id_provider').val();
    else 
        var value = $('#value_value').val();
    var search = $('#search').val();
    var type = $('#type').val();
    if(page != '' && page > 0){
        $('.loading').show(600);
        $.post('index.php/<?php echo $this->uri->segment(1);?>/ajax/'+value+'/'+search, {
            view: 'jump_page',
            page: page,
            type: type
            
        }, function(data){
            $('.loading').hide(600);
            $('#idList').empty().html(data);
        }, 'html');
    } else return;
}

function editobj(idboj)
{
        $('.loading').show(600);
        $.post('index.php/<?php echo $this->uri->segment(1);?>/ajax', {
            view: 'view_edit',
            idboj: idboj
            
        }, function(data){
            $('.loading').hide(600);
            $('#idDetail').empty().html(data);
        }, 'html');
    
}
function search()
{  
   if($('#id_provider').length)
        var value = $('#id_provider').val();
    else 
        var value = $('#value_value').val();
    
   var search = $('#search').val();
    if(value > 0)
   {
        $('.loading').show(600);
        $.post('index.php/<?php echo $this->uri->segment(1);?>/ajax/', {
            view: 'search_search',
            value: value,
            search:search 
            
        }, function(data){
            $('.loading').hide(600);
            $('#idList').empty().html(data);
        }, 'html');
    } else return;
}

function control(value)
{
	var id_obj = 0;
	var name_value = '';
	var arry_value = value.split('-');
	id_obj = arry_value[1];
	name_value = arry_value[0];
	switch(name_value)
	{
		case 'edit' : 
			var modalLocation = 'blogChitiet';//$(this).attr('data-reveal-id');
			$('#'+modalLocation).reveal($(this).data());
			editobj(id_obj);
			break;
		case 'delete' : publish(2,id_obj);break;
		case 'publish' : publish(1,id_obj);break;
		case 'unpublish' : publish(0,id_obj);break;
	}
	$('#control-'+id_obj).val('');
	
	 
}
function publish(status,id_obj){ // status = 1:publish     status = 0: unpublish
	page = $('#page').val();
	name = $('#name_'+id_obj).text();
	 
    if(status == 1){
        var title = 'Publish Provider ';
        var warn = 'Are you sure to want <b>Publish</b> Provider  <b>["'+name+'"]</b> ?';
    } else
	if(status == 2){
        var title = 'Delete Provider ';
        var warn = 'Are you sure to want <b>delete</b> Provider  <b>["'+name+'"]</b> ?';
    } else {
        var title = 'Unpublish Provider ';
        var warn = 'Are you sure to want <b>Unpublish</b> Provider  <b>["'+name+'"]</b> ?';
    }
    $.confirm({
		'title'		: title,
		'message'	: warn,
		'buttons'	: {
			'Yes'	: {
				'class'	: 'blue',
				'action': function(){
					$.post('index.php/<?php echo $this->uri->segment(1);?>/ajax', {
                        view: 'publish_obj', 
                        id_obj: id_obj,
                        value: status,
						page : page
                    }, function(data){
						$('.loading').hide(600);
						$('#idList').empty().html(data);
					}, 'html');
				}
			},
			'No'	: {
				'class'	: 'gray',
				'action': function(){}
			}
		}
	});
}
</script>