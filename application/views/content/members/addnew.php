<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 <!-- bodyMain -->
        <div class="pageMain">
        	
            <!-- main left -->
            <?php $view_left = $this->uri->segment(1); $this->load->view('left/'.$view_left); ?>
            <!-- en main left -->
            
            <!-- main right -->
            <div class="mainRight">
            	
               
                <!-- content -->
                
                <div class="blogContent">
                	
                    <div class="rowHeader rowHeader2 fixed">
                    	<i class="icon iconTaomoi"></i> Add new 
                    </div>
                    
                    <div class="boxContent">
                    <?php echo validation_errors('<div class="error_ci">', '</div>'); ?>	 
					<?php echo $this->session->userdata('mess') ; $this->session->unset_userdata('mess'); ?>
						<?php 
						$attributes = array('id' => 'myform');
						echo form_open('members/addnew',$attributes); 
						?>
                        
                        <!-- en box tieu de -->
                         <div class="boxFill">
                        	<a href="#" class="icon minF"></a>
                        	<h4 class="title">Info for Login</h4>
                            <div class="contentFill formFill contT">
                            	 <label>Fullname <span class="red">(*)</span></label>
                                
                        		<input type="text" name="name" id="name" value="<?php echo set_value('name'); ?>" class="inp inpTitle"  />
                                <br />
                                
                                <label>Email Address <span class="red">(*)</span></label>
                                 
                        		<input type="text" name="email" id="email" value="<?php echo set_value('email'); ?>" class="inp inpTitle"  />
                            	<br />
                                <label>Password <span class="red">(*)</span></label>
                                 
                        		<input type="password" id="password" name="password" value="<?php echo set_value('password'); ?>" class="inp inpTitle"  />
                                 <br />
                                <label>Password Confirm <span class="red">(*)</span></label>
                                 
                        		<input type="password" name="passconf" id="passconf" value="<?php echo set_value('passconf'); ?>" class="inp inpTitle"  />
                                <br />
                                
                                
                            </div>  
                        </div>
                         
                        <div class="bntBottom">
                        	<input type="submit" value="Submit" class="bntAll"  />
                        	 
                        </div>
                        
                        <div class="padT10"></div>
                       
                    </div>
                
                </div>
                 
                <!-- en content -->
            
            </div>
            <!-- en main right -->
            
            <div class="clr"></div>
        
        </div>
        <!-- en bodyMain -->   
        
        <!-- footer -->
        <div class="footerAdIn">Copyright 2012 - 2013 <span>AAN</span>. Allright services</div>
        <!-- en footer --> 
        
    </div>

</body>
</html>
<script type="text/javascript">
$(function(){
   $('#myform1').validate({
        rules: {
            'name': { required: true },
            'email': { required: true, email: true },
            'password': { required: true, minlength: 6, maxlength: 20},
            'passconf': { required: true, minlength: 6, maxlength: 20, equalTo: '#password' },
            
        },
        errorElement: "div",
		errorPlacement: function(error, element) { 
			offset = element.offset();    
			error.insertBefore(element);  
			error.css({'position': 'absolute', 'left': offset.left + element.outerWidth() - 100, 'top': offset.top - $('.error').height() - 20 });
		},
        messages: {
            'name': { required: 'Please enter full name.' },
            'email': { required: 'Please enter email login.', email: 'email invalid format.' },
            'password': { required: 'Please enter password.', minlength: 'minimum is 6 character.', maxlength: 'maximum is 20 character.' },
            'passconf': { required: 'Please enter password.', minlength: 'minimum is 6 character.', maxlength: 'maximum is 20 character.', equalTo: 'Comfirm password not the same password above.' },
            
        }
   });
   
    
});
</script>