<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="boxContent">
<?php  echo validation_errors('<div class="error_ci">', '</div>'); 
echo $this->session->userdata('mess') ; $this->session->unset_userdata('mess'); ?>
	<?php 
    $attributes = array('id' => 'myform');
    echo form_open('members/view_edit',$attributes); 

	?>
	
	<!-- box tieu de -->
	<div class="boxFill">
		<a href="#" class="icon minF"></a>
		<h4 class="title">Info <?php echo $obj->fullname;?></h4>
		<div class="contentFill formFill contT">
			 
			<label>Name <span class="red">(*)</span></label>
			<input type="text" name="name" id="name" 
                    value="<?php if(!empty($obj->fullname)) echo $obj->fullname; else echo set_value('name'); ?>" 
                    class="inp inpTitle"  readonly="readonly" />
                    
            <label>Email <span class="red">(*)</span></label>
            <input type="text" name="email" id="email" 
                    value="<?php if(!empty($obj->email)) echo $obj->email; else echo set_value('email'); ?>" 
                    class="inp inpTitle"  readonly="readonly" />
                    
            <label>Pass <span class="red">(*)</span></label>
            <input type="text" name="password" id="password" 
                    value="<?php echo set_value('password'); ?>" 
                    class="inp inpTitle" />
            <label>Again Pass <span class="red">(*)</span></label>
            <input type="text" name="passconf" id="passconf" 
                    value="<?php echo set_value('passconf'); ?>" 
                    class="inp inpTitle" />
                    
            
            
            
            
            <input type="hidden" name="idboj" id="idboj" value="<?php echo $obj->id;?>" />
			<br />
			
			<br />
			
		</div>  
	</div>
	<!-- en box tieu de -->

	<div class="bntBottom">
         <input type="submit" value="Submit" class="bntAll"/>
    </div>
	
	<div class="padT10"></div>
   
</div>
<script type="text/javascript">

$(function(){
   $('#myform').validate({
        rules: {
            
            'name': { required: true },
            'email': { required: true, email: true },
            'password': { required: true, minlength: 6, maxlength: 20},
            'passconf': { required: true, minlength: 6, maxlength: 20, equalTo: '#password' },
        },
        errorElement: "div",
		errorPlacement: function(error, element) { 
			offset = element.offset();    
			error.insertBefore(element);  
			error.css({'position': 'absolute', 'left': offset.left + element.outerWidth() - 300, 'top': offset.top - $('.error').height() - 90 });
		},
        messages: {
           'name': { required: 'Please enter full name.' },
            'email': { required: 'Please enter email login.', email: 'email invalid format.' },
            'password': { required: 'Please enter password.', minlength: 'minimum is 6 character.', maxlength: 'maximum is 20 character.' },
            'passconf': { required: 'Please enter password.', minlength: 'minimum is 6 character.', maxlength: 'maximum is 20 character.', equalTo: 'Comfirm password not the same password above.' },
            
        }
   });
   
    
});
</script>