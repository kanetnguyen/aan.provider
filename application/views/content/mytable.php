<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 <!-- bodyMain -->
        <div class="pageMain">
        	
            <!-- main left -->
            <?php $view_left = $this->uri->segment(1); $this->load->view('left/'.$view_left); ?>
            <!-- en main left -->
            
            <!-- main right -->
            <div class="mainRight">
            	
               
                <!-- content -->
                
                <div class="blogContent">
                	
                    <div class="rowHeader rowHeader2 fixed">
                    	<i class="icon iconTaomoi"></i> Table
                    </div>
                    
                    <div class="boxContent">
                    	<?php echo $this->table->generate($static); ?>
                        <div class="padT10"></div>
                       <?php
					 		
					   $tmpl = array (
										'table_open'          => '<table cellpadding="10" cellspacing="0" border="1" width="100%" class="tableComment">',
					
										'heading_row_start'   => '<tr class="rowH">',
										'heading_row_end'     => '</tr>',
										'heading_cell_start'  => '<th>',
										'heading_cell_end'    => '</th>',
					
										'row_start'           => '<tr>',
										'row_end'             => '</tr>',
										'cell_start'          => '<td>',
										'cell_end'            => '</td>',
					
										'row_alt_start'       => '<tr>',
										'row_alt_end'         => '</tr>',
										'cell_alt_start'      => '<td>',
										'cell_alt_end'        => '</td>',
					
										'table_close'         => '</table>'
								  );
					
						$this->table->set_template($tmpl); 

						$this->table->set_heading('ID', 'Name', 'Intro');
						 
						echo $this->table->generate($product); 
					   ?>
                    </div>
                
                </div>
                 
                <!-- en content -->
            
            </div>
            <!-- en main right -->
            
            <div class="clr"></div>
        
        </div>
        <!-- en bodyMain -->   
        
        <!-- footer -->
        <div class="footerAdIn">Copyright 2012 - 2013 <span>AAN</span>. Allright services</div>
        <!-- en footer --> 
        
    </div>

</body>
</html>
 