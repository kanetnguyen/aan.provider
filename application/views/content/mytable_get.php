<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 <!-- bodyMain -->
        <div class="pageMain">
        	
            <!-- main left -->
            <?php $view_left = $this->uri->segment(1); $this->load->view('left/'.$view_left); ?>
            <!-- en main left -->
            
            <!-- main right -->
            <div class="mainRight">
            	
               
                <!-- content -->
                
                <div class="blogContent">
                	
                    <div class="rowHeader rowHeader2 fixed">
                    	<i class="icon iconTaomoi"></i> Table phân trang GET
                    </div>
                    
                    <div class="boxContent">
                    	 
                       <?php
					 		
					   $tmpl = array (
										'table_open'          => '<table cellpadding="5" cellspacing="0" border="" width="95%" class="tableComment">',
					
										'heading_row_start'   => '<tr class="rowH">',
										'heading_row_end'     => '</tr>',
										'heading_cell_start'  => '<th>',
										'heading_cell_end'    => '</th>',
					
										'row_start'           => '<tr>',
										'row_end'             => '</tr>',
										'cell_start'          => '<td>',
										'cell_end'            => '</td>',
					
										'row_alt_start'       => '<tr>',
										'row_alt_end'         => '</tr>',
										'cell_alt_start'      => '<td>',
										'cell_alt_end'        => '</td>',
					
										'table_close'         => '</table>'
								  );
					
						$this->table->set_template($tmpl); 

						$this->table->set_heading('ID', 'Name', 'Intro','Control');
						 foreach($product as $pro)
 						{  
							$control = '<ul>
											<li><a data-reveal-id="blogChitiet" href="myform/edit/'.$pro->id.'"><i class="icon iconChoBienTap"></i> Edit</a></li>
											<a href="javascript:publish(1);"><i class="icon iconGuiLen"></i> Publish </a>
											<li><a href="myform/publish/'.$pro->id.'"><i class="icon iconGoBo"></i> UnPublish</a></li>
											<li><a href="myform/delete/'.$pro->id.'"><i class="icon iconXoaTam"></i> Delete</a></li>
										</ul>';
							 
 							$this->table->add_row( $pro->id, $pro->title, $pro->intro,$control);
 						}
						//set footer
						 
						$cell = array('data' => $paging, 'class' => 'highlight', 'colspan' => 6);
						$this->table->add_row($cell);
 
						echo $this->table->generate(); 
						 
					   ?>
                    </div>
                
                </div>
                 
                <!-- en content -->
            
            </div>
            <!-- en main right -->
            
            <div class="clr"></div>
        
        </div>
        <!-- en bodyMain -->   
        
        <!-- footer -->
        <div class="footerAdIn">Copyright 2012 - 2013 <span>AAN</span>. Allright services</div>
        <!-- en footer --> 
        
    </div>
<!-- light box -->
<div id="blogChitiet" class="reveal-modal boxChiTiet">
    <a class="close-reveal-modal"></a>
    <div class="boxIn contentBI">
        <h4 class="titleBox">Tiêu đề</h4> 
        <div class="contentBox">Nội dung</div>
    </div>
</div>
<!-- en light box -->
</body>
</html>
<script>
function publish(status){ // status = 1:publish     status = 0: unpublish
    if(status == 1){
        var title = 'Publish LiveTV';
        var warn = 'Are you sure to want <b>Publish</b> LiveTV <b>["abc"]</b> ?';
    } else
	if(status == 2){
        var title = 'Delete LiveTV';
        var warn = 'Are you sure to want <b>delete</b> LiveTV <b>['+$('#row'+curentRow+' td:eq(2) h4').text()+']</b> ?';
    } else {
        var title = 'Unpublish LiveTV';
        var warn = 'Are you sure to want <b>Unpublish</b> LiveTV <b>['+$('#row'+curentRow+' td:eq(2) h4').text()+']</b> ?';
    }
    $.confirm({
		'title'		: title,
		'message'	: warn,
		'buttons'	: {
			'Yes'	: {
				'class'	: 'blue',
				'action': function(){
					$.post('index.php/mytable/ajax', {
                        view: 'publish', 
                        pid: $('#row'+curentRow+' td:first').find(':hidden').val(),
                        value: status
                    }, function(data){ CheckPers(); // this function check permission for call function php with ajax in background
                        if(data){ 
                            if($('#curentShow').val() == 2){ // curent status is publish
                                var cTop = parseInt($('#numPages').text());     cTop = cTop - 1; // top and left unpublish
                                var cLeft = parseInt($('#unpubVod').text());    cLeft = cLeft + 1; // left publish
                                $('#pubVod').text(cTop); // set value for left publish
                                $('#numPages').text(cLeft); // set value for top
                                $('#unpubVod').text(cLeft); // set value for left unpublish
                                $('#row'+curentRow).remove(); // remove this tr from unpublish and add to publish
                            } else if($('#curentShow').val() == 3) { // curent status is unpublish 
                                var cTop = parseInt($('#numPages').text());     cTop = cTop - 1; // top and left unpublish
                                var cLeft = parseInt($('#pubVod').text());    cLeft = cLeft + 1; // left publish
                                $('#numPages').text(cTop); // set value for top
                                $('#unpubVod').text(cTop); // set value for left unpublish
                                $('#pubVod').text(cLeft); // set value for left publish
                                $('#row'+curentRow).remove(); // remove this tr from unpublish and add to publish
                            } else if($('#curentShow').val() == 4) { // curent status is delete 
                                var cTop = parseInt($('#numPages').text());     cTop = cTop - 1; // top and left unpublish
                                var cLeft = parseInt($('#delVod').text());    cLeft = cLeft - 1; // left publish
                                $('#delVod').text(cLeft); // set value for left publish
								$('#numPages').text(cTop); // set value for top
                                $('#row'+curentRow).remove(); // remove this tr from unpublish and add to publish
                            } else if($('#curentShow').val() == '' || $('#curentShow').val() == 1) { // for all then change status for this vod
                                if(status == 1){
                                    $('#row'+curentRow+' td:eq(1) i').removeClass('iconFlagOff').addClass('iconFlag');
                                    ($('#row'+curentRow).attr('rel') == 'p') ? $('#row'+curentRow).attr('rel', 'u') : $('#row'+curentRow).attr('rel', 'p');
                                } else {
                                    $('#row'+curentRow+' td:eq(1) i').removeClass('iconFlag').addClass('iconFlagOff');
                                    ($('#row'+curentRow).attr('rel') == 'p') ? $('#row'+curentRow).attr('rel', 'u') : $('#row'+curentRow).attr('rel', 'p');
                                }
                            }  
                        } else alert('Update status this VOD error, Please try again !');
                    }, 'text');
				}
			},
			'No'	: {
				'class'	: 'gray',
				'action': function(){}
			}
		}
	});
}
</script>