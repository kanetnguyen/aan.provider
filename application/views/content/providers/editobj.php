<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="boxContent">
                    	 
<?php echo $this->session->userdata('mess') ; $this->session->unset_userdata('mess'); ?>
	<?php 
	$attributes = array('id' => 'myform');
	echo form_open('providers/view_edit',$attributes); 
	?>
	
	<!-- box tieu de -->
	<div class="boxFill">
		<a href="#" class="icon minF"></a>
		<h4 class="title">Info <?php echo $obj->name;?></h4>
		<div class="contentFill formFill contT">
			 
			<label>Name <span class="red">(*)</span></label>
			<?php echo form_error('name', '<div class="error_ci">', '</div>'); //Gan div cho cụ thể báo lỗi nào ?>
			<input type="text" name="name" id="name" value="<?php if(!empty($obj->name)) echo $obj->name; else echo set_value('name'); ?>" class="inp inpTitle"  />
            <input type="hidden" name="idboj" id="idboj" value="<?php echo $obj->id;?>" />
			<br />
			
			<label>Type <span class="red">(*)</span></label>
			
			<select name="type" id="type" class="sel selN">
				<option value="1" <?php if($obj->level == 1) echo set_select('myselect', '1',TRUE); else echo set_select('myselect', '1'); ?> >
                AAN</option>
				<option value="2" <?php if($obj->level == 2) echo set_select('myselect', '2',TRUE); else echo set_select('myselect', '2'); ?> >
                VNPT-G(providers have access to the highest powers)</option>
				<option value="3" <?php if($obj->level == 1) echo set_select('myselect', '3',TRUE); else echo set_select('myselect', '3'); ?> >
                Provider normal</option>
			</select> 
			<br />
			
		</div>  
	</div>
	<!-- en box tieu de -->
	  
	<div class="bntBottom">
		<input type="submit" value="Submit" class="bntAll"  />
		 
	</div>
	
	<div class="padT10"></div>
   
</div>
<script type="text/javascript">

$(function(){
   $('#myform').validate({
        rules: {
            
            'name': { required: true }
        },
        errorElement: "div",
		errorPlacement: function(error, element) { 
			offset = element.offset();    
			error.insertBefore(element);  
			error.css({'position': 'absolute', 'left': offset.left + element.outerWidth() - 300, 'top': offset.top - $('.error').height() - 90 });
		},
        messages: {
           
            'name': { required: 'Please enter name.' }   
        }
   });
   
    
});
</script>