<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="boxContent">
                    	 
   <?php
    
    $this->table->set_heading('Publish', 'Name', 'Level','Control');
    foreach($product as $pro)
    {  
       if($pro->publish == 1)
            $str="<option value='unpublish-$pro->id'> UnPublish</option>";
        else
            $str="<option value='publish-$pro->id'> Publish</option>";
		 
		
        $control = '<select class="sel selN2" id="control-'.$pro->id.'"  onchange="control(this.value)">
                        <option value="">Mời chọn hành động</option>
                        <option value="edit-'.$pro->id.'"> Edit </option>
                        '.$str.'
                        <option value="delete-'.$pro->id.'">Delete</option>
                    </select>';
  
        if($pro->level == 1) $level = "AAN";
        else if($pro->level == 2) $level = "VNPT-G";
        else $level = "Provider Normal";
        
        if($pro->publish == 1) { $pub = '<i class="icon iconFlag"></i>'; $st = 'p'; } 
        else { $pub = '<i class="icon iconFlagOff"></i>'; $st = 'u'; }
        
        $this->table->add_row(array($pub, "<label id='name_".$pro->id."'>". $pro->name."</label>", $level,$control));
        
		 
    }
	
	 
    //set footer
     
    $cell = array('data' => $paging, 'class' => 'highlight', 'colspan' => 6);
    $this->table->add_row($cell);

    echo $this->table->generate(); 
     
   ?>
</div>