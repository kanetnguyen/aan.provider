<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 <!-- bodyMain -->
        <div class="pageMain">
        	
            <!-- main left -->
            <?php $view_left = $this->uri->segment(1); $this->load->view('left/'.$view_left); ?>
            <!-- en main left -->
            
            <!-- main right -->
            <div class="mainRight">
            	
               
                <!-- content -->
                
                <div class="blogContent">
                	
                    <div class="rowHeader rowHeader2 fixed">
                    	<i class="icon iconTaomoi"></i> Manager Provider
                    </div>
                    <div id="idList">
                    <?php echo $this->session->userdata('mess') ; $this->session->unset_userdata('mess'); ?>
                    <?php
					$data['product'] = $product;
					 
					$data['paging'] = $paging;
					 
					$this->load->view('content/providers/load_ajax',$data);
					?>
                    
                	</div>
                </div>
                 
                <!-- en content -->
            
            </div>
            <!-- en main right -->
            
            <div class="clr"></div>
        
        </div>
        <!-- en bodyMain -->   
        
        <!-- footer -->
        <div class="footerAdIn">Copyright 2012 - 2013 <span>AAN</span>. Allright services</div>
        <!-- en footer --> 
        
    </div>

 <!-- light box -->
<div id="blogChitiet" class="reveal-modal boxChiTiet">
    <a class="close-reveal-modal"></a>
    <div class="boxIn contentBI">
        <h4 class="titleBox">Edit</h4> 
        <div id="idDetail">
        	<div class="contentBox">Nội dung</div>
        </div>
    </div>
</div>
<!-- en light box -->
</body>
</html>
<script>
function jumpPage(page)
{
	 
    if(page != '' && page > 0){
         
        $('.loading').show(600);
        $.post('index.php/<?php echo $this->uri->segment(1);?>/ajax', {
            view: 'jump_page',
            page: page
            
        }, function(data){
            $('.loading').hide(600);
            $('#idList').empty().html(data);
        }, 'html');
    } else return;
}

function editobj(idboj)
{
        $('.loading').show(600);
        $.post('index.php/<?php echo $this->uri->segment(1);?>/ajax', {
            view: 'view_edit',
            idboj: idboj
            
        }, function(data){
            $('.loading').hide(600);
            $('#idDetail').empty().html(data);
        }, 'html');
    
}

function control(value)
{
	var id_obj = 0;
	var name_value = '';
	var arry_value = value.split('-');
	id_obj = arry_value[1];
	name_value = arry_value[0];
	switch(name_value)
	{
		case 'edit' : 
			var modalLocation = 'blogChitiet';//$(this).attr('data-reveal-id');
			$('#'+modalLocation).reveal($(this).data());
			editobj(id_obj);
			break;
		case 'delete' : publish(2,id_obj);break;
		case 'publish' : publish(1,id_obj);break;
		case 'unpublish' : publish(0,id_obj);break;
	}
	$('#control-'+id_obj).val('');
	
	 
}
function publish(status,id_obj){ // status = 1:publish     status = 0: unpublish
	page = $('#page').val();
	name = $('#name_'+id_obj).text();
	 
    if(status == 1){
        var title = 'Publish Provider ';
        var warn = 'Are you sure to want <b>Publish</b> Provider  <b>["'+name+'"]</b> ?';
    } else
	if(status == 2){
        var title = 'Delete Provider ';
        var warn = 'Are you sure to want <b>delete</b> Provider  <b>["'+name+'"]</b> ?';
    } else {
        var title = 'Unpublish Provider ';
        var warn = 'Are you sure to want <b>Unpublish</b> Provider  <b>["'+name+'"]</b> ?';
    }
    $.confirm({
		'title'		: title,
		'message'	: warn,
		'buttons'	: {
			'Yes'	: {
				'class'	: 'blue',
				'action': function(){
					$.post('index.php/<?php echo $this->uri->segment(1);?>/ajax', {
                        view: 'publish_obj', 
                        id_obj: id_obj,
                        value: status,
						page : page
                    }, function(data){
						$('.loading').hide(600);
						$('#idList').empty().html(data);
					}, 'html');
				}
			},
			'No'	: {
				'class'	: 'gray',
				'action': function(){}
			}
		}
	});
}
</script>