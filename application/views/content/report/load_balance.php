<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="rowTable">
    <div class="rowIn">
        <span class="colName">Tiền Thật </span>  
        <span class="colNum"><?php echo round($balance,3);?> USD</span>
        <a href="javascript:xl_detail('balance')" class="icon iconXem">Xem</a>
        <a href="javascript:xl_export('pdf','balance')" class="icon iconXem">Pdf</a>
        <a href="javascript:xl_export('excel','balance')" class="icon iconXem">Excel</a>
    </div>
    
    <div class="rowIn">
        <span class="colName">Tiền ảo </span> 
        <span class="colNum"><?php echo round($promotion,3);?> USD</span>
        <a href="javascript:xl_detail('promotion')" class="icon iconXem">Xem</a>
        <a href="javascript:xl_export('pdf','promotion')" class="icon iconXem">Pdf</a>
        <a href="javascript:xl_export('excel','promotion')" class="icon iconXem">Excel</a>
     </div>
</div>