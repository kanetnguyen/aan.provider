<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<style type="text/css">

.rowTable { margin:20px 20px 20px 0; background:#fff; width:590px; padding:5px; box-shadow:5px 5px 10px #ccc }
.colName { display:inline-block; background:#ccc; padding:5px; width:180px; font-weight:bold;}
.colNum { display:inline-block; width:220px; padding:5px; text-align:center; margin-right:0px; background:#F6F6F6}
.rowIn { border-bottom:1px solid #fff}
.rowIn a { padding-left:20px; margin-left:10px}
.display {display:block;}
.nondisplay {display:none;}
</style>
 <!-- bodyMain -->
        <div class="pageMain">
        	
            <!-- main left -->
            <?php $view_left = $this->uri->segment(1); $this->load->view('left/'.$view_left); ?>
            <!-- en main left -->
            
            <!-- main right -->
            <div class="mainRight">
            	
               
                <!-- content -->
                
                <div class="blogContent">
                	 
                    <div style="clear:both; margin-bottom:20px"></div>
                    
                    <div class="rowHeader rowHeader2 fixed">
                    	<i class="icon iconTaomoi"></i> 
                        <?php
                        if(isset($loai))
                        {
                            if($loai==0)
                                echo "Doanh Thu Thuê Bao ";
                            else
                                echo "Doanh Thu Bán Lẻ ";
                        }
                        ?>
                        Theo thời gian </div>
                        <input type="hidden" id="type" name="type" value="<?php echo isset($type)?$type:"a"; ?>" />
                        <input type="hidden" id="loai" name="loai" value="<?php echo isset($loai)?$loai:"a"; ?>" />
                    
                    
                         Từ ngày <input type="text" name="date_to" id="date_to" value="<?php echo set_value('search'); ?>" class="inp"  />
                         &nbsp; &nbsp; Đến ngày 
                         <input type="text" name="date_from" id="date_from" value="<?php echo set_value('search'); ?>" class="inp"  />
                           &nbsp; &nbsp;
                         <div class="bntAll" onclick="xl_tracuu();">Tra cứu</div>
                                <br /><br />
                         
                    <div id="total_report">
                    
                    	<?php
                        if(isset($balance) && isset($promotion))
                        {
                            $data['balance'] = $balance ;
    						$data['promotion'] = $promotion ;
                            $data['type'] = $type ;
    						echo $this->load->view('content/report/load_balance',$data); 
                        }
						?>
                    </div>
                    
                    
                    </div>
                    <div id="detail_report" class="nondisplay" > 
                	</div>
                    
                    

                    
                    
                </div>
                 
                <!-- en content -->
            
            </div>
            <!-- en main right -->
            
            <div class="clr"></div>
        
        </div>
        <!-- en bodyMain -->   
        
        <!-- footer -->
        <div class="footerAdIn">Copyright 2012 - 2013 <span>AAN</span>. Allright services</div>
        <!-- en footer --> 
        
    </div>

 <!-- light box -->
<div id="blogChitiet" class="reveal-modal boxChiTiet">
    <a class="close-reveal-modal"></a>
    <div class="boxIn contentBI">
        <h4 class="titleBox">Edit</h4> 
        <div id="idDetail">
        	<div class="contentBox">Noi dung</div>
        </div>
    </div>
</div>
<!-- en light box -->
</body>
</html>
<script>
function xl_detail(type)
{
	$('#detail_report').removeClass('nondisplay');
	$('#detail_report').addClass('display');
	date_from = $('#date_from').val();
	date_to = $('#date_to').val();
    loai = $('#loai').val();
	$('#type').val(type);
    $('#form').text(type);
	$('.loading').show(600);
	$.post('index.php/<?php echo $this->uri->segment(1);?>/ajax/', {
		view : 'detail',
		type : type,
		date_to : date_to,
		date_from : date_from,
        loai:loai
		
	}, function(data){
		$('.loading').hide(600);
		$('#detail_report').empty().html(data);
	}, 'html');
	
}
function xl_tracuu()
{
	date_from = $('#date_from').val();
	date_to = $('#date_to').val();
	loai = $('#loai').val();
	$('#detail_report').removeClass('display');
	$('#detail_report').addClass('nondisplay');
	
	$('.loading').show(600);
	$.post('index.php/<?php echo $this->uri->segment(1);?>/ajax/', {
		view : 'tracuu',
		date_to : date_to,
		date_from : date_from,
        loai:loai
		
	}, function(data){
		$('.loading').hide(600);
		$('#total_report').empty().html(data);
	}, 'html');
	
}
function xl_export(file,type)
{
	
	$('.loading').show(600);
	$.post('index.php/<?php echo $this->uri->segment(1);?>/ajax/', {
		view : 'export',
		type : type,
		file : file
		
	}, function(data){
		$('.loading').hide(600);
		$('#detail_report').empty().html(data);
	}, 'html');
	
}

function jumpPage(page)
{
    var date_from = $('#date_from').val();
	var date_to = $('#date_to').val();
    var type = $('#type').val();
    var loai = $('#loai').val();
    if(page != '' && page > 0){
        $('.loading').show(600);
        $.post('index.php/<?php echo $this->uri->segment(1);?>/ajax/'+type, {
            view: 'jump_page',
            date_to : date_to,
            date_from : date_from,
            page: page,
            loai:loai
            
        }, function(data){
            $('.loading').hide(600);
            $('#idList').empty().html(data);
        }, 'html');
    } else return;
}
 
function view_edit(idboj)
{
        $('.loading').show(600);
        $.post('index.php/<?php echo $this->uri->segment(1);?>/ajax', {
            view: 'view_edit',
            idboj: idboj
            
        }, function(data){
            $('.loading').hide(600);
            $('#idDetail').empty().html(data);
        }, 'html');
}

function control(value)
{
	var id_obj = 0;
    var vip = 0;
	var name_value = '';
	var arry_value = value.split('-');
	id_obj = arry_value[1];

	name_value = arry_value[0];
	switch(name_value)
	{
		case 'edit' : 
			var modalLocation = 'blogChitiet';//$(this).attr('data-reveal-id');
			$('#'+modalLocation).reveal($(this).data());
			view_edit(id_obj);
			break;
        
	}
	$('#control-'+id_obj).val('');	 
}
</script>