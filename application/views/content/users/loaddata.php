<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 <!-- bodyMain -->
        <div class="pageMain">
        	
            <!-- main left -->
            <?php $view_left = $this->uri->segment(1); $this->load->view('left/users'); ?>
            <!-- en main left -->
            
            <!-- main right -->
            <div class="mainRight">
            	
               
                <!-- content -->
                
                <div class="blogContent">
                	
                    <div class="rowHeader rowHeader2 fixed">
                    	<i class="icon iconTaomoi"></i> Search User !!
                    </div>
         
                     
                                   <input type="text" name="search" id="search" value="<?php echo set_value('search'); ?>" class="inp inpTitle"  />
                                   
                                <div class="bntAll" onclick="chose();">Tìm kiếm</div>
                                <br /><br />
                    <div id="idList">
                    <?php 
                        echo $this->session->userdata('mess') ; $this->session->unset_userdata('mess'); 
    					$data['product'] = $product;
    					$data['paging'] = $paging;
    					$this->load->view('content/users/load_ajax',$data);
					?>
                	</div>
                </div>
                 
                <!-- en content -->
            
            </div>
            <!-- en main right -->
            
            <div class="clr"></div>
        
        </div>
        <!-- en bodyMain -->   
        
        <!-- footer -->
        <div class="footerAdIn">Copyright 2012 - 2013 <span>AAN</span>. Allright services</div>
        <!-- en footer --> 
        
    </div>

 <!-- light box -->
<div id="blogChitiet" class="reveal-modal boxChiTiet">
    <a class="close-reveal-modal"></a>
    <div class="boxIn contentBI">
        <h4 class="titleBox">Edit</h4> 
        <div id="idDetail">
        	<div class="contentBox">N?i dung</div>
        </div>
    </div>
</div>
<!-- en light box -->
</body>
</html>
<script>
function jumpPage(page)
{
    var search = $('#search').val();
    if(page != '' && page > 0){
        $('.loading').show(600);
        $.post('index.php/<?php echo $this->uri->segment(1);?>/ajax/'+search, {
            view: 'jump_page',
            page: page, 
            
        }, function(data){
            $('.loading').hide(600);
            $('#idList').empty().html(data);
        }, 'html');
    } else return;
}
function jumpPageAjax(page,idboj,type)
{
    if(page != '' && page > 0){
        $('.loading').show(600);
        $.post('index.php/<?php echo $this->uri->segment(1);?>/ajax/', {
            view: 'view_edit',
            page: page, 
            idboj: idboj,
            type: type
            
        }, function(data){
            $('.loading').hide(600);
            $('#idListAjax').empty().html(data);
        }, 'html');
    } else return;
}
function chose()
{  
   var search = $('#search').val();
    if(search != "")
   {
        $('.loading').show(600);
        $.post('index.php/<?php echo $this->uri->segment(1);?>/ajax/', {
            view: 'choseee',
            search:search 
            
        }, function(data){
            $('.loading').hide(600);
            $('#idList').empty().html(data);
        }, 'html');
    } else return;
}
function view_edit(idboj,type)
{
        $('.loading').show(600);
        $.post('index.php/<?php echo $this->uri->segment(1);?>/ajax', {
            view: 'view_edit',
            idboj: idboj,
            type: type
            
        }, function(data){
            $('.loading').hide(600);
            $('#idDetail').empty().html(data);
        }, 'html');
}

function control(value)
{
	var id_obj = 0;
    var vip = 0;
	var name_value = '';
	var arry_value = value.split('-');
	id_obj = arry_value[1];
    type = arry_value[2];
	name_value = arry_value[0];
	switch(name_value)
	{
		case 'edit' : 
			var modalLocation = 'blogChitiet';//$(this).attr('data-reveal-id');
			$('#'+modalLocation).reveal($(this).data());
			view_edit(id_obj,type);
			break;
        
	}
	$('#control-'+id_obj).val('');	 
}
</script>