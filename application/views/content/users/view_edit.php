<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="boxContent">
<?php  echo validation_errors('<div class="error_ci">', '</div>'); 
echo $this->session->userdata('mess') ; $this->session->unset_userdata('mess'); ?>
	<?php 
    $attributes = array('id' => 'myform');
    echo form_open('users/view_edit',$attributes); 

	?>
	
	<!-- box tieu de -->
	<div class="boxFill">
		<a href="#" class="icon minF"></a>
		  <div id="idListAjax">
              <?php
              
                            $this->load->library('Thuvien');
                            
                            $paging =$this->thuvien->paging_ajax($total,$numrow,$page,$idopj,$type); 
              //=====================================================================================
              switch($type)
              {
                case 1:  // Show Chi Tiet
                    if($total>0)
                    {
                        ?>
                        <h4 class="title">Thông Tin Cá Nhân</h4>
                        <div class="contentFill formFill contT">
                			<input type="hidden" name="idboj" id="idboj" value="<?php echo $obj[0]['id'];?>" />
                			<br />
                			<label>Email : <span class="red"><?php echo $obj[0]['email'];?></span></label> 
                            <label>Full Name: <span class="red"><?php echo $obj[0]['fullname']; ?></span></label> 
                            <label>Vip: <span class="red"><?php echo ($obj[0]['vip']==1)?"Yes":"No"; ?></span></label>     
                			<label>Phone: <span class="red"><?php echo $obj[0]['phone']; ?></span></label>
                            <label>Last Login: <span class="red"><?php echo $obj[0]['last_login']; ?></span></label>
                            <br />
                            <label>Đã Mua<div class="red">
                                <?php
                                    foreach($obj as $value)
                                    {
                                        $result=$this->Modelusers->get_name_provider($value['pro']);
                                        echo (count($result)>0)?($result->name."<br>"):""; 
                                    }
                                ?>
                            </div></label>
                        </div>
                        <?php  
                    }
                    else
                        echo "Không Có Dữ Liệu";
                    break;
                case 2:    // Show LOgin
                    if($total>0)
                    {
                        ?>
                        <h4 class="title">Log Đăng Nhập</h4>
                        <div class="contentFill formFill contT">
            			<input type="hidden" name="idboj" id="idboj" value="<?php echo $obj[0]['id'];?>" />
            			<br />
            			<label>Email : <span class="red"><?php echo $obj[0]['email'];?></span></label> 
                        <label>Full Name: <span class="red"><?php echo $obj[0]['fullname']; ?></span></label> 
                        <br />
                        <label>List Login : </label>
                            <?php
                            $this->table->set_heading('Login Time', 'Logout Time','Ip');
                                foreach($obj as $value)
                                    $this->table->add_row(array($value['date_login'],$value['date_logout'],$value['ip']));
                                $cell = array('data' => $paging, 'class' => 'highlight', 'colspan' => 3);
                                $this->table->add_row($cell);
                            echo $this->table->generate(); 
                            ?>
                        </div>
                        <?php  
                    }
                    else
                        echo "Không Có Dữ Liệu";
                    break;
                case 3:  // Show Buy
                    if($total>0)
                    {
                        ?>
                        <h4 class="title">Log Buy</h4>
                        <div class="contentFill formFill contT">
                        <br />
                        <label>List Buy : </label>
                            <?php
                             
                            $this->table->set_heading('Name', 'Provider Name','Date','Expired Date','Image','Price');
                                foreach($obj as $value)
                                    $this->table->add_row(array($value['name'],$value['loainame'],$value['date'],$value['expired_date'],"<img height='75px' src='".IMG_FILM.$value['image']."' />",$value['price']));
                                $cell = array('data' => $paging, 'class' => 'highlight', 'colspan' => 6);
                                $this->table->add_row($cell);
                            echo $this->table->generate(); 
                            ?>
                        </div>
                        <?php  
                    }
                    else
                        echo "Không Có Dữ Liệu";
                    break;
                case 4:   // Log Nap Tien
                    if($total>0)
                    {
                        ?>
                        <h4 class="title">Log Nạp</h4>
                        <div class="contentFill formFill contT">
                        <br />
                        <label>List Nạp : </label>
                            <?php
                            $this->table->set_heading('Charge _ From', 'Amount','Date','Description');
                                foreach($obj as $value)
                                    $this->table->add_row(array($value['charge_from'],$value['amount'],$value['date'],$value['description']));
                                $cell = array('data' => $paging, 'class' => 'highlight', 'colspan' => 4);
                                $this->table->add_row($cell);
                            echo $this->table->generate(); 
                            ?>
                        </div>
                        <?php  
                    }
                    else
                        echo "Không Có Dữ Liệu";
                    break;
              }
			     ?>
			<br />
			
		</div>  
        </div>
	</div>
	<!-- en box tieu de -->

	<div class="padT10"></div>
   
</div>
