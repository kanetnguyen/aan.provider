<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<base href="<?php echo base_url(); ?>" /> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:::: Providers Accessasia ::::</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/init.css" rel="stylesheet" type="text/css" />
<link href="css/jquery.confirm.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/custom_validate.css" /> 

<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/library.js"></script>


<script type="text/javascript" src="scripts/tytabs.jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#tabsholder").tytabs({
			fadespeed:"fast"
							});
});
</script>

<script type="text/javascript" src="scripts/vscontext.jquery.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
            $('#context1').vscontext({menuBlock: 'vs-menu1'});
			$('#context2').vscontext({menuBlock: 'vs-menu2'});
	});
</script>

<link rel="stylesheet" href="css/reveal.css">	
<script type="text/javascript" src="scripts/jquery.validate.js" ></script>
<script type="text/javascript" src="scripts/jquery.reveal.js"></script>
<script type="text/javascript" src="scripts/jquery.confirm.js"></script>
<script type="text/javascript" src="scripts/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="scripts/functions.js"></script> 
</head>

<!-- script for tiny_mce editor -->
<script language="javascript" type="text/javascript" src="/libraries/tinymce/tinymce/js/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({
 mode : "textareas",
 theme : "advanced",
 plugins : "table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,zoom,flash,searchreplace,print,contextmenu",
 theme_advanced_buttons1_add_before : "save,separator",
 theme_advanced_buttons1_add : "fontselect,fontsizeselect",
 theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,zoom,separator,forecolor,backcolor",
 theme_advanced_buttons2_add_before: "cut,copy,paste,separator,search,replace,separator",
 theme_advanced_buttons3_add_before : "tablecontrols,separator",
 theme_advanced_buttons3_add : "emotions,iespell,flash,advhr,separator,print",
 theme_advanced_toolbar_location : "top",
 theme_advanced_toolbar_align : "left",
 theme_advanced_path_location : "bottom",
 plugin_insertdate_dateFormat : "%Y-%m-%d",
 plugin_insertdate_timeFormat : "%H:%M:%S",
 extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
 relative_urls : false
});
</script>
<!-- end script -->

<body>
<?php 
    

//Kiem tra login
if($this->session->userdata('admin_id') < 1) redirect('/login/', 'refresh'); 
$res = $this->commondb->permission_provider($this->session->userdata('level_provider'),$this->uri->segment(1));  
if($res == 0)redirect('', 'refresh'); 

?>
    
	<div class="siteAd siteLog"> 
    	
        <!-- header -->
        <div class="headerAd">
        	<a href="#" class="logo"><img src="images/logo.png" /></a>
            <div class="blogUser">
            	Hello <b><?php echo($this->session->userdata('username')) ;  ?></b> <a href="members/logout" class="icon icoLogout" title="Logout">Logout</a>  | <a href="" class="icon icoChangepass"  data-reveal-id="blogMatKhau" title="Change password">Change password</a>

                <!-- light box -->
                
                <div id="blogMatKhau" class="reveal-modal boxGhiChu blogFrom">
                <form id="myform">
                    <a class="close-reveal-modal"></a>
                    <div class="boxIn">
                        <h4 class="titleBox">Đổi mật khẩu</h4> 
                        <div class="contentBox boxForm">
                        	<label>Mật khẩu cũ</label><input type="pass" class="inp inpLog" value="<?php echo($this->session->userdata('password')) ;  ?>"/><br />
                            <label>Mật khẩu mới</label><input type="text" id="password" class="inp inpLog" value="<?php echo set_value('password'); ?>" /><br />
                            <label>Gõ lại mật khẩu mới</label><input type="text" id="passconf" class="inp inpLog" value="<?php echo set_value('passconf'); ?>" /><br />
                            <label> </label><input type="submit" value="Đồng ý" class="bntAll" />
                        </div>
                    </div>
                </div>
                </form>
                <!-- en light box -->

                
            </div>
        </div>
        <!-- en header -->
        
        <!-- nav -->
        <div class="navTop">
        	<ul>
            	<li><a href=""><span>Dashboard</span></a></li>
                <?php if($this->session->userdata('level_provider') == 1) { ?>
                <li><a href="providers" <?php if($this->uri->segment(1)=='providers') echo "class='active'"; ?> >
                    <span>Providers</span></a></li>
                <?php } ?>
                <?php  if($this->session->userdata('level_provider') < 3 ) { ?>
                <li><a href="users" <?php if($this->uri->segment(1)=='users') echo "class='active'"; ?> >
                    <span>Members AAN</span></a></li>
                <?php } ?>
                <li><a href="members" <?php if($this->uri->segment(1)=='members') echo "class='active'"; ?> >
                    <span>My Users</span></a></li>
                
                <li><a href="movies" <?php if($this->uri->segment(1)=='movies') echo "class='active'"; ?> >
                    <span>Movies</span></a></li>
                <?php  if($this->session->userdata('level_provider') < 3 ) { ?>
                <li><a href="livetv" <?php if($this->uri->segment(1)=='livetv') echo "class='active'"; ?> >
                    <span>LiveTV</span></a></li>
                <?php } ?>
                <li><a href="report" <?php if($this->uri->segment(1)=='report') echo "class='active'"; ?> >
                    <span>Report</span></a></li>
            </ul>
            <div class="clr"></div>
        </div>
        <!-- en nav -->
<script>
$(function(){
   $('#myform').validate({
        rules: {
            'password': { required: true, minlength: 6, maxlength: 20},
            'passconf': { required: true, minlength: 6, maxlength: 20, equalTo: '#password' },
        },
        errorElement: "div",
		errorPlacement: function(error, element) { 
			offset = element.offset();    
			error.insertBefore(element);  
			error.css({'position': 'absolute', 'left': offset.left + element.outerWidth() - 300, 'top': offset.top - $('.error').height() - 90 });
		},
        messages: {
            'password': { required: 'Please enter password.', minlength: 'minimum is 6 character.', maxlength: 'maximum is 20 character.' },
            'passconf': { required: 'Please enter password.', minlength: 'minimum is 6 character.', maxlength: 'maximum is 20 character.', equalTo: 'Comfirm password not the same password above.' },
            
        }
   });
   
    
});

</script>