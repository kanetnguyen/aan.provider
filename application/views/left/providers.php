<!-- main left -->
<div class="mainLeft">
    
    <!-- categries -->
    <div class="categories">
         
        <ul>
            <li><a href="providers/add" <?php if($this->uri->segment(2)=='add') echo "class='active'"; ?>>
            	<i class="icon iconQLQC"></i> Add new Provider</a></li> 
             <li><a href="providers" <?php if($this->uri->segment(2)=='') echo "class='active'"; ?>>
            	<i class="icon iconQLQC"></i> Manager Providers</a></li> 
              
        </ul>
    </div>
    <!-- en categries -->

</div>
<!-- en main left -->