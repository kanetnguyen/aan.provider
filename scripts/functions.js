/* 
    popup function, use id div to show
    and default class="popup" to hide before reshow
    <div class="popup" id="test" width="500px" height="400px">-- content here --</div>
    <a href="javascript:popup('test');">show popup</a>
*/
function popup_div(id){
    var w = $(window).width() / 2;
    var h = $(window).height() / 2;
    var top = h - $('#'+id).height() / 2;
    var left = w - $('#'+id).width() / 2;
    $('.popup').hide();
    $('#'+id).css({'top': top +'px', 'left': left+'px', 'position': 'fixed', 'z-index': 5000}).show(600);
}
/* 
    close popup 
    <div class="popup" id="test" width="500px" height="400px">
        <a href="javascript:close('close_btn');" id="close_btn">close</a>
    -- content here --</div>
*/
// $(document).keypress(function(e){ if(e.keyCode == 27) code here });  event press esc button
function close(id){
    var parent_id = $('#'+id).parent().attr('id');
    $('#'+parent_id).hide(500);
}
/*
use function isNumber and isLetters to validate only keypress number and letters
<input type="text" onkeypress="return isNumber(event)" /> in html
*/
function isNumber(event){
	var charcode = window.event ? event.keyCode : event.which;
	//if(charcode > 31 && charcode != 78 && charcode != 89 && charcode != 110 && charcode != 121)
	if(charcode > 31 && (charcode < 48 || charcode > 57))
		return false;
	return true;
}
function isLetters(event){
	var charcode = window.event ? event.keyCode : event.which;
	if(charcode > 32 && (charcode < 65 || charcode > 90) && (charcode < 97 || charcode > 122))
		return false;
	return true;
}
function isFloat(event){
    var charcode = window.event ? event.keyCode : event.which;
	if(charcode != 46 && charcode > 31 && (charcode < 48 || charcode > 57))
		return false;
	return true;
    /* ^[0-9]{1,2}([,.][0-9]{1,2})?$
    ^[0-9]+[.]?[0-9]*$
    var numbers =/^[0-9]+(\.[0-9]+)+$/;  
    if((inputtxt.value.match(numbers)) { return true; }  */
  
}
/*
    function checkBoxAll tobe checked/unchecked multiple checkbox with onclick checkbox parent
    classChild2 can empty(have when it's child's classChild1)
    using this function in html 
    <input type="checkbox" id="parentId" onclick="checkBoxAll('parentId', 'classChild')" /> checkbox parent text
        <input type="checkbox" class="classChild" /> checkbox child text
        <input type="checkbox" class="classChild" /> checkbox child text
*/
function checkBoxAll(parentId, classChild1, classChild2){
    var status = $("#"+parentId).attr('checked');
    $("."+classChild1).each(function() { this.checked = status; });
    if(classChild2 != '') $("."+classChild2).each(function() { this.checked = status; });
}
/*
    function show contextmenu custom with rightclick
    this function need id's element rightclick and classname div or other elemets tobe show
    use this function:
        <tr id="r1" onmousedown="return RightClick('r1', 'Rmenu');"><td>row 1</td></tr>
        <div class="Rmenu" style="display:none;">conten for context menu here</div>
            id's: r1
            Classname's: Rmenu
*/
function RightClick(idClick, classDiv){
	$('#'+idClick).bind("contextmenu", function(e){ 
		$('.'+classDiv).hide();
		if(e.button == 2){										
			$('.'+classDiv).show('slow');
			$('.'+classDiv).css({'display': 'block', 'top': e.pageY, 'left': e.pageX, 'position': 'absolute'});									
		} else { $('.'+classDiv).hide('slow'); }
		return false; // disable contextmenu default of browser
	}); 
	$('.'+classDiv).hover(function(){}, function(){$('.'+classDiv).hide('slow');})
}
/*  this function create tabs with jquery
    it use class div, class ul add css class to tag a then active div
    use this function:
        ===============================html=============================
        <ul class="tabs" >
          <li><a rel="tab1" class="active" href="javascript:void(0);">Manager</a></li>  
          <li><a rel="tab2" href="javascript:void(0);">eidt</a></li>
        </ul> 
        <div class="tab_container" >
            <div id="tab1" class="tab_content">manager content div</div> 
            <div id="tab2" class="tab_content">edit content div</div>
        </div>
        ===============================call and use in code================================
        class div: tab_content
        class ul: tabs
        css class: active
        <script type="text/javascript">
            $(function(){ 
                CreateTab('tab_content', 'tabs', 'active');
            });
        </script>
*/
function CreateTab(classDIV, classUL, classCSS){
    $('.'+classDIV+':not(.'+classDIV+':first)').hide(); 
	//$('ul.'+classUL+' li:first').addClass(classCSS).fadeIn(); 
	$('ul.'+classUL+' li a').live('click',function() {
		  $('ul.'+classUL+' li a').removeClass(classCSS);						   
		  //$(this).find('a').addClass(classCSS);  
          $(this).addClass(classCSS);
		  //var activeTab = $(this).find('a').attr('rel');
          var activeTab = $(this).attr('rel');
		  $('.'+classDIV).hide();		
		  $('#'+activeTab).delay(400).fadeIn();
	});
}
/*
    
        
*/

/*
    function setup editor with ckeditor
    function need id's textarea, width, height
    how to use it ?
        CreateEditor('editor', 500, 300);
        editor: id's textarea
*/    
var editorObj;
function CreateEditor(id, width, height){
	editorObj = CKEDITOR.replace(id, {
		height: height,
        width: width,
		/*uiColor: '#9D4F00',
        contentsCss: 'content.css',*/
        //skin: 'moono-light',
		enterMode: CKEDITOR.ENTER_BR,
		//language: 'vi',
        filebrowserBrowseUrl : '../scripts/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : '../scripts/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl : '../scripts/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl : '../scripts/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '../scripts/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '../scripts/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
        toolbar: [
    		[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
            [ 'Link', 'Unlink', 'Anchor' ],
            [ 'Image', 'Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ],
            [ 'Maximize' ],
            [ 'Source' ],
            [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ],
            [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ],
            [ 'Styles','Format','Font','FontSize' ],
			[ 'TextColor','BGColor' ]
    	]
    });
}
/*
    function refresh new captcha
*/
function new_captcha(path, id){ 
    var c_currentTime = new Date();
    var c_miliseconds = c_currentTime.getTime();
    document.getElementById(id).src = path+'captcha.php?x='+ c_miliseconds;
}
/* 
    editor tinymce 
    Javascript for TBL
    how to use it ?
        tbl.editor('description', 990, 350);
        description: id's textarea
*/
var tbl = {
	editor: function(id, w, h) {
		tinyMCE.init({
			mode : "exact",
			elements : id,
			theme : "advanced",
            width: w,
            height: h,
			plugins : "style,table,advimage,advlink,insertdatetime,preview,flash,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,imagemanager,safari,layer,media,advhr,emotions,iespell,preview,xhtmlxtras,inlinepopups",
			theme_advanced_buttons1_add_before : "save,newdocument,separator",
			theme_advanced_buttons1_add : "fontselect,fontsizeselect",
			theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,separator,forecolor,backcolor",
			//theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
			theme_advanced_buttons3_add_before : "tablecontrols,separator",
			theme_advanced_buttons3_add : "emotions,iespell,flash,advhr,separator,print,separator,ltr,rtl,separator,fullscreen",
			//theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,teaser",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_path_location : "bottom",
			plugin_insertdate_dateFormat : "%Y-%m-%d",
			plugin_insertdate_timeFormat : "%H:%M:%S",
			extended_valid_elements : "hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
			file_browser_callback : "ajaxfilemanager",
			theme_advanced_resize_horizontal : false,
			theme_advanced_resizing : true
		});
	}, // Action
	deleted: function(href, title) {
		if(confirm(title)) {
			document.myform.action = href;
			document.myform.submit();
			return true;
		} else return false;
	},	
	button: function(href) {
		document.myform.action = href;
		document.myform.submit();
		return true;
	}
} // end tinymce editor
/*
    function link video
*/
function link_video(name) {
	document.myform.http240.value = 'http://221.133.7.133:1935/tivididong/mp4:'+name+'.240p.mp4/playlist.m3u8';
	document.myform.http360.value = 'http://221.133.7.133:1935/tivididong/mp4:'+name+'.360p.mp4/playlist.m3u8';
	document.myform.http480.value = 'http://221.133.7.133:1935/tivididong/mp4:'+name+'.480p.mp4/playlist.m3u8';
	document.myform.http720.value = 'http://221.133.7.133:1935/tivididong/mp4:'+name+'.720p.mp4/playlist.m3u8';
	document.myform.http1080.value = 'http://221.133.7.133:1935/tivididong/mp4:'+name+'.1080p.mp4/playlist.m3u8';
	document.myform.rtmp240.value = name+'.240p';
	document.myform.rtmp360.value = name+'.360p';
	document.myform.rtmp480.value = name+'.480p';
	document.myform.rtmp720.value = name+'.720p';
	document.myform.rtmp1080.value = name+'.1080p';
}
/*
 function get link vod
*/
function get_link() {	
    link = document.myform.link.value;
	name_file = document.myform.name_file.value;
	name_stream = document.myform.name_stream.value;
	document.myform.rtmphost.value = 'rtmp://'+ link + '/'+ name_stream ;
	if(document.myform.check_1.checked == true){
		document.myform.http240.value = 'http://'+link+"/"+name_stream+"/mp4:"+name_file+'.240p/playlist.m3u8';
		document.myform.rtmp240.value = name_file+'.240p';
	} else {
		document.myform.http240.value = '';
		document.myform.rtmp240.value = '';
	}
	if(document.myform.check_2.checked == true){
		document.myform.http360.value = 'http://'+link+"/"+name_stream+"/mp4:"+name_file+'.360p/playlist.m3u8';
		document.myform.rtmp360.value = name_file+'.360p';
	} else {
	    document.myform.http360.value = '';
		document.myform.rtmp360.value = '';
	}
	if(document.myform.check_3.checked == true){
		document.myform.http480.value = 'http://'+link+"/"+name_stream+"/mp4:"+name_file+'.480p/playlist.m3u8';
		document.myform.rtmp480.value = name_file+'.480p';
	} else {
		document.myform.http480.value = '';
		document.myform.rtmp480.value = '';
	}
	if(document.myform.check_4.checked == true){
		document.myform.http720.value = 'http://'+link+"/"+name_stream+"/mp4:"+name_file+'.720p/playlist.m3u8';
		document.myform.rtmp720.value = name_file+'.720p';
	} else {
		document.myform.http720.value = '';
		document.myform.rtmp720.value = '';
	}
	if(document.myform.check_5.checked == true){
		document.myform.http1080.value = 'http://'+link+"/"+name_stream+"/mp4:"+name_file+'.1080p/playlist.m3u8';
		document.myform.rtmp1080.value =  name_file+'.1080p';
	} else {
		document.myform.http1080.value = '';
		document.myform.rtmp1080.value =  '';
	} 
}
// Select all checkboxes
$(document).ready(function(){
    /*$("#checkboxall").click(function(){
        var checked_status = this.checked;
        $("input[name=checkall]").each(function() {
            this.checked = checked_status;
        });
    });*/
});	
//load video
function loadVideo(containerVideo, flashDiv, w, h, rtmpHost, rtmp240p, rtmp360p, rtmp480p, rtmp720p, rtmp1080p, iosUrl){
	var container=document.getElementById('' + containerVideo);
	if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
		container.innerHTML = '<video id="ios-player" width="'+w+'px" height="'+h+'px" controls="controls" preload="auto"><source src="'+iosUrl+'" type="video/mp4"/></video>';
	} else {
		container.innerHTML = '<object width="'+ w +'" height="'+ h +'" type="application/x-shockwave-flash" id="smartplayer" data="player/smartplayer.swf">'
            +'<param name="src" value="player/smartplayer.swf">'
            +'<param name="menu" value="false">'
            +'<param name="scale" value="noScale">'
            +'<param name="allowFullscreen" value="true">'
            +'<param name="allowScriptAccess" value="always">'
            +'<param name="salign" value="tl">'
            +'<param name="bgcolor" value="#000000">'
            +'<param name="wmode" value="transparent" />'   
            +'<param name="flashvars" value="configUrl=player/config.xml&amp;logoImage=player/resources/images/non_logo.png&amp;playerWidth='+ w +'&amp;playerHeight='+ h +'&amp;isFullScreen=1&amp;mediaPathUrl='+ rtmpHost +',mp4:'+ rtmp240p +',mp4:'+ rtmp360p +',mp4:'+ rtmp480p +',mp4:'+ rtmp720p +',mp4:'+ rtmp1080p +'"></object>';
	}	
}0
//load video
function loadVideoNoLogo(containerVideo, flashDiv, w, h, rtmpHost, rtmp240p, rtmp360p, rtmp480p, rtmp720p, rtmp1080p, iosUrl){
	var container=document.getElementById('' + containerVideo);
	if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
		container.innerHTML='<video id="ios-player" width="'+w+'px" height="'+h+'px" controls="controls" preload="auto"><source src="'+iosUrl+'" type="video/mp4"/></video>';
	} else {
		container.innerHTML='<object width="'+ w +'" height="'+ h +'" type="application/x-shockwave-flash" id="smartplayer" data="player/smartplayer.swf"><param name="src" value="player/smartplayer.swf"><param name="menu" value="false"> <param name="scale" value="noScale">  <param name="allowFullscreen" value="true">    <param name="allowScriptAccess" value="always">    <param name="salign" value="tl">    <param name="bgcolor" value="#000000"> <param name="wmode" value="transparent" />   <param name="flashvars" value="configUrl=player/config.xml&amp;logoImage=player/resources/images/non_logo.png&amp;playerWidth='+ w +'&amp;playerHeight='+ h +'&amp;isFullScreen=1&amp;mediaPathUrl='+ rtmpHost +',mp4:'+ rtmp240p +',mp4:'+ rtmp360p +',mp4:'+ rtmp480p +',mp4:'+ rtmp720p +',mp4:'+ rtmp1080p +'"></object>';
	}	
}
// create cookie

//var now = new Date();
//var time = now.getTime(); time += 3600 * 1000; now.setTime(time); 
//document.cookie = 'linkT=' + indexT + '; expires=' + now.toGMTString() + '; path=/';
function getCookie(c_name) {
	var i, x, y, ARRcookies = document.cookie.split(';');
	for (i = 0; i < ARRcookies.length; i++) {
	  x = ARRcookies[i].substr(0, ARRcookies[i].indexOf('='));
	  y = ARRcookies[i].substr(ARRcookies[i].indexOf('=')+1);
	  x = x.replace(/^\s+|\s+$/g, '');
	  if (x == c_name) { return unescape(y); }
	}
}

function setCookie(c_name, value, exdays) {
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value = escape(value) + ((exdays == null) ? '' : '; expires = ' + exdate.toUTCString());
	document.cookie = c_name + '=' + c_value;
}
// end section cookie
/*      
        rules: { 
		},
        // set position message error 
		errorElement: "div",
		errorPlacement: function(error, element) { 
			offset = element.offset();    
			error.insertBefore(element);  
			error.css({'position': 'absolute', 'left': offset.left + element.outerWidth() - 31, 'top': offset.top - $('.error').height() - 20 });
		},
        // end section show message error
		messages :{           
		}
        
    custome functions for validate jquery
    count number word in textarea
    use 
        rules: {
           'id_textarea': { wordCount: ['150'] }
		},
*/
 
/*
    set focus and blur with element
    
    $(function(){
        $(".defaultText").focus(function(){
            if ($(this).val() == $(this)[0].title) {
                $(this).removeClass("defaultTextActive");
                $(this).val('');
            }
        });
        
        $(".defaultText").blur(function(){
            if ($(this).val() == ''){
                $(this).addClass("defaultTextActive");
                $(this).val($(this)[0].title);
            }
        });    
        $(".defaultText").blur();        
    });
*/
